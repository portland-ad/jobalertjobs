<?php

namespace App\Controller;

use App\Entity\Experience;
use App\Entity\JobCategory;
use App\Entity\Label;
use App\Entity\Role;
use App\Repository\JobCategoryRepository;
use App\Service\AdminService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\FOSRestController;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/admin/jobs", name="admin")
 */
class AdminController extends AbstractController
{

    /**
     * @Route("/label", name="create_jobalert_label", methods={"POST"})
     * @param Request $request
     * @param AdminService $adminService
     * @return Response
     */
    public function createNewJobLabel(Request $request, AdminService $adminService): Response
    {
        $em = $this->getDoctrine()->getManager();
       try{
           $requestData = json_decode($request->getContent(), true);
           $label = $adminService->saveJobLableData(json_encode( $requestData));
           $em->flush($label);
           return $this->json(['status'=>'ok', 'label'=>$label],201);
       } catch (\Exception $e)
       {
           return $this->json(['status'=>'error', 'message'=>$e->getMessage()],500);
       }
    }

    /**
     * @Route("/role", name="create_jobalert_role", methods={"POST"})
     * @param Request $request
     * @param AdminService $adminService
     * @return Response
     */
    public function createNewJobRole(Request $request, AdminService $adminService): Response
    {

        $em = $this->getDoctrine()->getManager();
        try{
            $requestData = json_decode($request->getContent(), true);
            $label = $adminService->saveJobRoleData(json_encode( $requestData));
            $em->flush($label);
            return $this->json(['status'=>'ok', 'label'=>$label],201);
        } catch (\Exception $e)
        {
            return $this->json(['status'=>'error', 'message'=>$e->getMessage()],500);
        }


        $response = new Response();
        $requestData = json_decode($request->getContent(), true);
        $savedLableResponse = $adminService->saveJobRoleData(json_encode( $requestData));

        if($savedLableResponse->validationFlag)
        {
            return $response->setContent(json_encode([
                'data' => "Data is saved successfully", 'errors' => null, 'validiationError'=> null
            ]));
        }
        else
        {
            return $response->setContent(json_encode([
                'data' => null , 'errors' => null, 'validiationError'=> serialize($savedLableResponse->validiationError)
            ]));
        }
    }

    /**
     * @Route("/category", methods={"POST"})
     * @param Request $request
     * @param JobCategoryRepository $jobCategoryRepo
     * @return Response
     */
    public function createNewJobCategory(Request $request, JobCategoryRepository $jobCategoryRepo):Response
    {


        $em = $this->getDoctrine()->getManager();
        try{
            $requestData = json_decode($request->getContent(), true);
            $categoryName = $requestData['name'];
            $jobCategory = $jobCategoryRepo->findOneBy(['name'=>$categoryName]);
            if($jobCategory)
            {
                throw new \Exception($categoryName . " already exist");
            }
            $jobCategory = new JobCategory();
            $jobCategory->setName($categoryName);
            $jobCategory->setStatus(true);
            $em->persist($jobCategory);
            $em->flush();

            return $this->json(['status'=>'ok','job-category'=>$jobCategory],201);
        }
        catch (\Exception $e)
        {
          return $this->json(['status'=>'error','message'=>$e->getMessage()],500);
        }
    }

    /**
     * @Route("/", name="all_info")
     * @param Request $request
     * @return Response
     */
    public function gelAllInfo(Request $request):Response
    {
        $em = $this->getDoctrine()->getManager();
        try{

            $jobCategory = $em->getRepository(JobCategory::class)->findAll();
            $jobRole = $em->getRepository(Label::class)->findAll();
            $jobLabel = $em->getRepository(Role::class)->findAll();
            $experience = $em->getRepository(Experience::class)->findAll();
            $data = new stdClass();
            $data->jobCategory = $jobCategory;
            $data->jobRole = $jobRole;
            $data->jobLabel = $jobLabel;
            $data->experience = $experience;

            return $this->json(['status'=>'ok', 'data'=>['job_category'=>$jobCategory, 'job_role'=>$jobRole,'job_label'=> $jobLabel, 'experience' => $experience]],200);
        } catch (\Exception $e)
        {
            return $this->json(['status'=>'error', 'message'=>$e->getMessage()],500);
        }
    }
}
