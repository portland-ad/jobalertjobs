<?php


namespace App\Service;


use App\Entity\Label;
use App\Entity\Role;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerBuilder;
use stdClass;

class AdminService
{
    private $em;
    private $serializer;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->serializer = SerializerBuilder::create()->build();

    }

    public function saveJobLableData($request)
    {
        $labelRepo = $this->em->getRepository(Label::class);
        $requestData = json_decode($request);
        $labelName = isset($requestData->name)? $requestData->name:null;

        if(!$labelName){
            throw new \Exception('Label Name can not be empty');
        }

        $label = $labelRepo->findOneBy(['name'=>$labelName]);
        if($label)   throw new \Exception('Label Already Exist');

        $label = new Label();
        $label->setName($labelName);
        $label->setCreateAt(new \DateTime());
        $label->setIsActive(true);
        $this->em->persist($label);
        return $label;

    }

    public function saveJobRoleData($request)
    {
        $roleRepo = $this->em->getRepository(Role::class);
        $requestData = json_decode($request);
        $roleName = isset($requestData->name)? $requestData->name:null;

        if(!$roleRepo){
            throw new \Exception('Role Name can not be empty');
        }

        $role = $roleRepo->findOneBy(['name'=>$roleName]);
        if($role)  throw new \Exception('Role Already Exist');

        $role = new Role();
        $role->setName($roleName);
        $role->setCreateAt(new \DateTime());
        $role->setIsActive(true);
        $this->em->persist($role);
        return $role;
    }
}