<?php


namespace App\Controller;


use App\Entity\JobEducationReq;
use App\Entity\JobType;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class JobEducationReqController extends FOSRestController
{
    /**
     * @Route("jobs/education-requirement/{id}",methods={"GET"})
     * @param Request $request
     * @param string $id
     * @return Response
     */
    public function getEducationReq(Request $request, string $id):Response
    {
        $jobEducatonRepo = $this->getDoctrine()->getManager()->getRepository(JobEducationReq::class);
        try{
            $educatoinalReq = $jobEducatonRepo->find($id);
            if(!$educatoinalReq) throw new \Exception("no data exist");
            return $this->handleView($this->view(['status'=>'ok','job_education_req'=>$educatoinalReq],Response::HTTP_OK));
        } catch (\Exception $exception)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$exception->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("jobs/education-requirement/",methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function getAllEducationReq(Request $request):Response
    {
        $jobEducatonRepo = $this->getDoctrine()->getManager()->getRepository(JobEducationReq::class);
        try{
            $educatoinalReq = $jobEducatonRepo->findAll();
            return $this->handleView($this->view(['status'=>'ok','job_education_reqs'=>$educatoinalReq],Response::HTTP_OK));
        } catch (\Exception $exception)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$exception->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("jobs/education-requirement/{id}",methods={"PUT"})
     * @param Request $request
     * @param string $id
     * @return Response
     */
    public function updateEducationReq(Request $request,string $id):Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        try{
            $educatoinalReq = $entityManager->getRepository(JobEducationReq::class)->find($id);
            $requestData = json_decode($request->getContent(), true);
            $educatoinalReq->setDegreTitle($requestData['degre_title']);
            $entityManager->flush();
            return $this->handleView($this->view(['status'=>'ok','job_education_req'=>$educatoinalReq],Response::HTTP_OK));
        } catch (\Exception $exception)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$exception->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("jobs/education-requirement/",methods={"POST"})
     * @param Request $request
     * @param string $id
     * @return Response
     */
    public function createEducationReq(Request $request):Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        try{
            $educatoinalReq = new JobEducationReq();
            $requestData = json_decode($request->getContent(), true);
            $educatoinalReq->setDegreTitle($requestData['degre_title']);
            $entityManager->persist($educatoinalReq);
            $entityManager->flush();
            return $this->handleView($this->view(['status'=>'ok','job_education_req'=>$educatoinalReq],Response::HTTP_CREATED));
        } catch (\Exception $exception)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$exception->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("jobs-type",methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function getAllJobTypes(Request $request):Response
    {
        $em = $this->getDoctrine()->getManager();
        try{
            $types = $em->getRepository(JobType::class)->findAll();
            return $this->handleView($this->view(['types'=>$types],Response::HTTP_OK));

        } catch (\Exception $e)
        {
            return $this->handleView($this->view(['types'=>'','status'=>'error','message'=>$e->getMessage()],Response::HTTP_OK));
        }
    }

    /**
     * @Route("jobs-type",methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function createJobTypes(Request $request):Response
    {
        $em = $this->getDoctrine()->getManager();
        try{
            $requestData = json_decode($request->getContent(), true);

            if($em->getRepository(JobType::class)->findOneBy(['type'=>$requestData['type']]))
            {
                throw new \Exception('Job type status code already exist');
            }
            $jobType = new JobType();
            $jobType->setTypeInText($requestData['type_in_text']);
            $jobType->setType($requestData['type']);
            $jobType->setCreateAt(new \DateTime());
            $em->persist($jobType);
            $em->flush();
            return $this->handleView($this->view(['types'=>$jobType],Response::HTTP_OK));
        } catch (\Exception $e)
        {
            return $this->handleView($this->view(['types'=>'','status'=>'error','message'=>$e->getMessage()],Response::HTTP_OK));
        }
    }

}