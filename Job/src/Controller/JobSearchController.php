<?php

namespace App\Controller;

use App\Repository\CitiesRepository;
use App\Repository\JobAlertUserRepository;
use App\Repository\JobSearchKeyWordRepository;
use App\Repository\JobsRepository;
use App\Service\JobsSearchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/job/search")
 */
class JobSearchController extends AbstractController
{
    /**
     * @Route("/job/search", name="job_search")
     */
    public function index()
    {
        return $this->render('job_search/index.html.twig', [
            'controller_name' => 'JobSearchController',
        ]);
    }

    /**
     * @Route("/findkeyword", name="job_search_keyword", methods={"POST"})
     * @param JobSearchKeyWordRepository $jobSearchKeyWordRepository
     * @param Request $request
     * @param JobsSearchService $jobsSearchService
     * @return Response
     * must be provided the keyword and validation will be in frontend side
     */
    public function findKeyword(JobSearchKeyWordRepository $jobSearchKeyWordRepository, Request $request, JobsSearchService $jobsSearchService): Response
    {
        $response = new Response();
            try{
                if(strpos($request->headers->get('content-type'),"application/json") > -1){
                    $requestData = json_decode($request->getContent(), true);
                    $data = $jobsSearchService->findJobSearchKeyWord(json_encode($requestData));
                    return $response->setContent(json_encode([
                        'data' => json_decode($data), 'errors' => null
                    ]));
                }
                else
                {
                    return $response->setContent(json_encode([
                        'data' => null, 'errors' => null
                    ]));
                }

            }catch (\Exception $e) {
                return $response->setContent(json_encode([
                    'data' => null, 'errors' => $e->getMessage()
                ]));
        }
    }

    /**
     * @Route("/findcity", name="job_search_find_city", methods={"POST"})
     * @param Request $request
     * @param JobsSearchService $jobsSearchService
     * @return Response
     * must be provided the city name
     */
    public function findCity(Request $request, JobsSearchService $jobsSearchService): Response
    {
        $response = new Response();
        try{
            if(strpos($request->headers->get('content-type'),"application/json") >= -1){
                $requestData = json_decode($request->getContent(), true);
                $data = $jobsSearchService->findJobSearchCityName(json_encode($requestData));
                return $response->setContent(json_encode([
                    'data' => json_decode($data), 'errors' => null
                ]));
            }
            else
            {
                return $response->setContent(json_encode([
                    'data' => null, 'errors' => null
                ]));
            }

        }catch (\Exception $e) {
            return $response->setContent(json_encode([
                'data' => null, 'errors' => $e->getMessage()
            ]));
        }
    }

    /**
     * @Route("/", name="job_search", methods={"GET"})
     * @param JobsRepository $jobsRepository
     * @param Request $request
     * @param JobsSearchService $jobsSearchService
     * @return Response
     */
    public function jobSearch(JobsRepository $jobsRepository, Request $request,  JobsSearchService $jobsSearchService): Response
    {

    }

}
