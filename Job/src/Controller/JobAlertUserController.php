<?php

namespace App\Controller;

use App\Entity\JobAlertUser;
use App\Entity\JobAlertUserEmailTracker;
use App\Form\JobAlertUserType;
use App\Repository\JobAlertUserRepository;
use App\Repository\JobCategoryRepository;
use App\Repository\JobsRepository;
use App\Service\JobsService;
use FOS\RestBundle\Controller\FOSRestController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/job-alerts")
 */
class JobAlertUserController extends FOSRestController
{
	const FRONTEND_URL = '';
    /**
     * @Route("/", name="job_alert_user_index", methods={"GET"})
     * @param JobAlertUserRepository $jobAlertUserRepository
     * @return Response
     */
    public function index(Request $request, JobAlertUserRepository $jobAlertUserRepository, JobsService $jobsService, PaginatorInterface $paginator): Response
    {


//        incompleted api will be do later

        $response = new Response();
        try{
            $page = $request->query->get('page',1);
            $limit = $request->query->get('limit',20);
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $email = $request->headers->get('ja-user-email');

            $paginated = $paginator->paginate($jobAlertUserRepository->findBy(['email'=>$email]),$page,$limit);
            $data = $paginated->getItems();
            $limit = $paginated->getItemNumberPerPage();
            $page = $paginated->getCurrentPageNumber();
            $totalItem = $paginated->getTotalItemCount();
            return $this->handleView($this->view(['status' => 'ok', 'data' => $data,'limit'=>$limit, 'page'=>$page, 'total_count'=>$totalItem], Response::HTTP_OK));
            }
            return $this->handleView($this->view(['status' => 'error', 'data' => null], Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
        catch (\Exception $e)
        {
            return $this->handleView($this->view(['status' => 'error', 'data' => null, 'message'=>$e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }

    }

    /**
     * @Route("/new", name="job_alert_user_new", methods={"GET"})
     * @param Request $request
     * @param JobsService $jobsService
     * @return Response
     */
    public function new(Request $request, JobsService $jobsService): Response
    {
        $response = new Response();
        try{

                $jobCategory = $jobsService->getAllJobCategory();
                return $response->setContent(json_encode([
                    'data' => json_decode($jobCategory) , 'errors' => null
                ]));

        }
        catch (\Exception $e)
        {
            return $response->setContent(json_encode([
                'data' => null, 'errors' => $e->getMessage()
            ]));
        }

    }

    /**
     * @Route("/new", name="save_alert", methods={"POST"})
     * @param Request $request
     * @param JobsService $jobsService
     * @return Response
     */
    public function create(Request $request, JobsService $jobsService): Response
    {

        $response = new Response();
        $em = $this->getDoctrine()->getManager();

        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $requestData = json_decode($request->getContent(), true);
                $data = $jobsService->saveJsonPostData(json_encode( $requestData));
                $jobsService->sendAlertConfirmationMail($data->getEmail(), $data->getIsRegister());
                $em->flush();

//                if( $data->getIsRegister())
//                {
//                    return $response->setContent(json_encode([
//                        'data' =>['email'=>$data->getEmail(),'status'=> 'active'] , 'errors' => null, 'message' => 'Active Job Alert created successfully. Check your email to register as a user'
//                    ]))->setStatusCode(201);
//                }

                return $response->setContent(json_encode([
                        'data' =>['email'=>$data->getEmail(),'status'=> 'active'] , 'errors' => null, 'message' => 'Job Alert successfully created'
                    ]))->setStatusCode(201);
            }
            else
            {
                $errors = "content type application/json expected";
                return $this->handleView($this->view(['status'=>'error','message'=> $errors],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
            }

        }
        catch (\Exception $e)
        {
            return $response->setContent(json_encode([
                'data' => null, 'errors' => $e->getMessage()
            ]));
        }

    }

    /**
     * @Route("/email/{email}/activate-register-status", name="activate-alert-register-status", methods={"GET"})
     * @param Request $request
     * @param JobsService $jobsService
     * @param string $email
     * @return Response
     * this function activate change all the  alert register status by this email
     */

    public function activatedJobAlertByEmail(Request $request, JobsService $jobsService, string $email):Response
    {
        $response = new Response();
        try {

            $jobsService->activateAlertUserRegisterStatus($email);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            return $response->setContent(json_encode([
                 'errors' => null, 'message' => 'Job Alert User Register activate successfully'
            ]))->setStatusCode(200);


        } catch (\Exception $e) {
            return $response->setContent(json_encode([
                'error'=> true, 'message' => $e->getMessage()
            ]));
        }
    }

    /**
     * @Route("/{id}/confirm", name="confirm_job_alert_mail", methods={"GET"})
     * @param Request $request
     * @param string $id
     * @param JobsService $jobsService
     * @return Response
     */
    public function confirmAlertUser(Request $request,string $id, JobsService $jobsService):Response
    {
        $response = new Response();
        try{
            $isActivated = $jobsService->activateAlertAfterUserCreation($id);
            $jobAlert = $this->getDoctrine()->getManager()->getRepository(JobAlertUser::class)->find($id);
            if($isActivated)
               {

                   $response->setContent(json_encode([ 'data' =>['email'=>$jobAlert->getEmail(),'status'=> 2, 'statusMessage'=> 'active'] , 'errors' => null, 'message' => 'Job ALert is activated successfully',
                   ]))->setStatusCode(Response::HTTP_OK);
               }
            else{
                    $response->setContent(json_encode([ 'data' =>null,'email'=>$jobAlert->getEmail(),  'errors' => "invalidEmail", 'message' => 'User Email is not registrared',
                    ]))->setStatusCode(Response::HTTP_BAD_REQUEST);
                }
            return $response;

        }
        catch (\Exception $e)
        {
            return $response->setContent(json_encode([
                'data' => null, 'errors' => $e->getMessage()
            ]));
        }

    }

    /**
     * @Route("/{id}", name="job_alert_user_show", methods={"GET"})
     */
    public function show(Request $request, JobAlertUserRepository $jobAlertUserRepository, String $id): Response
    {

        $response = new Response();
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $data = $jobAlertUserRepository->find($id);
                return $this->handleView($this->view(['status' => 'ok', 'data' => $data], Response::HTTP_OK));
            }
            return $this->handleView($this->view(['status' => 'error', 'data' => null], Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
        catch (\Exception $e)
        {
            return $this->handleView($this->view(['status' => 'error', 'data' => null, 'message'=>$e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("/{id}/edit", name="job_alert_user_edit", methods={"GET","POST","PUT"})
     * @param Request $request
     * @param JobAlertUser $jobAlertUser
     * @return Response
     */
    public function edit(Request $request, JobAlertUserRepository $jobAlertUserRepository, String $id, JobsService $jobsService): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = new Response();
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){

                $requestData = json_decode($request->getContent(), true);
                $data = $jobsService->saveJsonPostData(json_encode( $requestData), $id);

                $em->flush();
                $data = $jobAlertUserRepository->find($id);

                return $this->handleView($this->view(['status' => 'ok', 'data' => $data], 206));
            }
            return $this->handleView($this->view(['status' => 'error', 'data' => null], Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
        catch (\Exception $e)
        {
            return $this->handleView($this->view(['status' => 'error', 'data' => null, 'message'=>$e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("/{id}", name="job_alert_user_delete", methods={"DELETE"})
     * @param Request $request
     * @param JobAlertUser $jobAlertUser
     * @return Response
     */
    public function delete(Request $request,JobAlertUserRepository $jobAlertUserRepository, String $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $response = new Response();
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $data = $jobAlertUserRepository->find($id);
                $entityManager->remove($data);
                $entityManager->flush();
                return $this->handleView($this->view(['status' => 'ok', 'data' => null,'message'=>'alert deleted successfully'], 204));
            }
            return $this->handleView($this->view(['status' => 'error', 'data' => null], Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
        catch (\Exception $e)
        {
            return $this->handleView($this->view(['status' => 'error', 'data' => null, 'message'=>$e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("/send/email", name="send_email", methods={"GET"})
     * @param Request $request
     * @param JobsService $jobsService
     * @param JobsRepository $repository
     * @param PaginatorInterface $paginator
     * @param $page
     * @return Response
     */
    public function sendEmailDailyBasis(Request $request, JobsService $jobsService, JobsRepository $repository, PaginatorInterface $paginator):Response
    {
        $response = new Response();
        $em = $this->getDoctrine()->getManager();

        try{

        $em = $this->getDoctrine()->getManager();

        $emailTracker = $em->getRepository(JobAlertUserEmailTracker::class)->findAll()[0];

        $page = $emailTracker->getStartJobAlertPage();
        $limit = $emailTracker->getMaxLimit();
        $alertUserList =  $jobsService->getJobAlertUserList($page,$limit);


        foreach ($alertUserList as $alertUser)
        {
            $jobCategoryId = $alertUser->getJobCategoryId();
            $jobSentInfo = $alertUser->getCurrentAlertPageNum();
            $jobPage = null;
            $jobLimit = null;
            if($jobSentInfo == null)
            {
                $jobPage = 0;
                $jobLimit = 20;
            }
            else
            {
                $data =$jobSentInfo;
                $jobPage = $data['jobPage'];
                $jobLimit = 2;
                $alertUser->setCurrentAlertPageNum((array('jobPage'=>$jobPage+1)));

            }
            $jobsList = $jobsService->getJobAlertedPaginatedList($jobCategoryId, $jobPage+1, $jobLimit);
			
            $email = $alertUser->getEmail();
            $postedData = [];
			
			if(count($jobsList) > 0){
				
				foreach ($jobsList as $job)
				{
					$jobId = $job->getId();
					$jobTitle = $job->getJobTitle();
					$jobDetailsUrl = self::FRONTEND_URL.'/jobs/'.$jobId.'/details';

					$postedData[]= array('jobTitle'=>$jobTitle,'companyName'=>$job->getCompanyName(),'location'=>$job->getJobLocation(),'endAt'=>$job->getEndAt(),'maxSalary'=>$job->getMaxSalaryRange(),'minSalary'=>$job->getMinSalaryRange(),'skills'=>$job->getSkillQualificationRequirement(),'detailUrl'=>$jobDetailsUrl);

				}
				
				$postedData[]['showMore'] = self::FRONTEND_URL.'/search?q='.$alertUser->getJobCategoryName();
				$data = json_encode($postedData,true);
				$jobsService->sendJobToJobAlert($postedData, $email);
            }
            if(($jobPage*$jobLimit)<$jobsList->getTotalItemCount())
            {
                $jobPage = $jobPage + 1;
            }

            $alertUser->setCurrentAlertPageNum(['jobPage'=>$jobPage]);

        }
            $totalItemCount = $alertUserList->getTotalItemCount();
            if($page*$limit>= $totalItemCount) $page = 0;
            $emailTracker->setStartJobAlertPage(($page+1));
            $em->flush();
            return $response->setContent(json_encode([
                    'message' => 'success', 'errors' => null
                ]))->setStatusCode(Response::HTTP_OK);
        }
        catch (\Exception $e)
        {
            return $response->setContent(json_encode([
                'data' => null, 'errors' => $e->getMessage()
            ]))->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }
}
