<?php


namespace App\Controller;


use App\Service\JobsService;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CronsController extends FOSRestController
{
    /**
     * @Route("/cron/remove/job")
     * @param Request $request
     * @param JobsService $jobsService
     * @return Response
     */
    public function removeExpiredJob(Request $request, JobsService $jobsService):Response
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $data = $jobsService->removeExpiredJob();
            $em->flush();
            return $this->handleView($this->view(['data'=> $data],200));

        } catch (\Exception $exception){
            return $this->handleView($this->view(['error'=>$exception->getMessage()],500));
        }
    }
}