<?php

namespace App\Controller;

use App\Entity\JobAlertUser;
use App\Entity\JobCategory;
use App\Form\JobAlertUserType;
use App\Form\JobCategoryType;
use App\Repository\JobCategoryRepository;
use App\Service\JobsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/jobs-category")
 */
class JobCategoryController extends AbstractController
{
    /**
     * @Route("/", name="job_category_index", methods={"GET"})
     * @param JobsService $jobsService
     * @return Response
     */
    public function index(JobsService $jobsService): Response
    {
        $data = $jobsService->allJobCategoryJson();
        $response = new Response();
        return  $response->setContent(json_encode(['data'=>json_decode($data)]))->setStatusCode(Response::HTTP_OK);

    }

    /**
     * @Route("/new", name="job_category_get", methods={"GET"})
     */
    public function new(Request $request): Response
    {
        $jobCategory = new JobCategory();
        $form = $this->createForm(JobCategoryType::class, $jobCategory);
        $form->handleRequest($request);

        return $this->render('job_category/new.html.twig', [
            'job_category' => $jobCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="job_category_post", methods={"POST"})
     */
    public function new1(Request $request, JobsService $jobsService):Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $response = new Response();
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $requestData = json_decode($request->getContent(), true);
                $jobCategory = $jobsService->createNewJobCategory(json_encode( $requestData));
                $entityManager->flush();
                return $response->setContent(json_encode([
                    'data' => json_decode($jobCategory) , 'errors' => null
                ]));
            }
            else {
                $jobCategory = new JobCategory();
                $form = $this->createForm(JobCategoryType::class, $jobCategory);
                $form->handleRequest($request);
                $entityManager->persist($jobCategory);
                $entityManager->flush();
                return $this->redirectToRoute('job_category_index');
            }
        }
        catch (\Exception $e)
        {
            return $response->setContent(json_encode([
                'data' => null, 'errors' => $e->getMessage()
            ]));
        }
    }


    /**
     * @Route("/{id}", name="job_category_show", methods={"GET"})
     */
    public function show(JobCategory $jobCategory): Response
    {
        return $this->render('job_category/show.html.twig', [
            'job_category' => $jobCategory,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="job_category_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, JobCategory $jobCategory): Response
    {
        $form = $this->createForm(JobCategoryType::class, $jobCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('job_category_index');
        }

        return $this->render('job_category/edit.html.twig', [
            'job_category' => $jobCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="job_category_delete", methods={"DELETE"})
     */
    public function delete(Request $request, JobCategory $jobCategory): Response
    {
        if ($this->isCsrfTokenValid('delete'.$jobCategory->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($jobCategory);
            $entityManager->flush();
        }

        return $this->redirectToRoute('job_category_index');
    }

}
