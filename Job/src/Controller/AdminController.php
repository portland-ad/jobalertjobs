<?php


namespace App\Controller;


use App\Service\AdminService;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("job/admin")
 */
class AdminController extends FOSRestController
{
    /**
     * @Route("/job-category/new",name="create_job_category", methods={"POST"})
     * @param Request $request
     * @param AdminService $adminService
     */
    public function newJobCategory(Request $request, AdminService $adminService)
    {
        $entityManager = $this->getDoctrine()->getManager();
        try {
            if (strpos($request->headers->get('content-type'), "application/json") > -1) {
                $requestData = json_decode($request->getContent(), true);
                $userId = $request->headers->get('ja-user-id');
                $requestData['userId'] = $userId;
                $adminService->createNewJobCategory($requestData);
            }
        } catch (\Exception $ex)
        {

        }
    }
}