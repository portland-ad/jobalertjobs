<?php


namespace App\Controller\Admin;


use App\Entity\JobType;
use App\Entity\Label;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/jobs-type")
 */
class AdminJobTypeController extends AbstractFOSRestController
{
    /**
     * @Route("/new",  methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        try{

            if(strpos($request->headers->get('content-type'),"application/json") > -1) {
                $requestData = json_decode($request->getContent(), true);
                $jobType = new JobType();
                $jobType->setTypeInText($requestData['type_in_text']);
                $jobType->setCreateAt(new \DateTime());
                $jobType->setType($requestData['type']);
                $entityManager->persist($jobType);
                $entityManager->flush();
                return $this->handleView($this->view(['status' => 'ok', 'job_type' => $jobType], Response::HTTP_CREATED));
            }

            return $this->handleView($this->view(['status' => 'error', 'job_type' => null], Response::HTTP_UNSUPPORTED_MEDIA_TYPE));

        }
        catch (\Exception $e)
        {
            return $this->handleView($this->view(['status' => 'error', 'job_type' => null,'message'=> $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("/{id}",name="edit_job_type",methods={"PUT"})
     * @param Request $request
     * @param String $id
     * @return Response
     */
    public function editJobType(Request $request, String $id)
    {
        $em = $this->getDoctrine()->getManager();
        $jobTypeRepo = $em->getRepository(JobType::class);
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $jobType = $jobTypeRepo->find($id);
                $requestData = json_decode($request->getContent(), true);
                $jobTypeInText = isset($requestData['type_in_text'])?$requestData['type_in_text']:$jobType->getTypeInText();
                $jobType->setTypeInText($jobTypeInText);
                $em->flush();
                return $this->handleView($this->view(['status'=>'ok', 'job_type' =>$jobType],206));
            }
            return $this->handleView($this->view(['status'=>'error'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
        catch (\Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'job_category' =>null],500));
        }
    }


}