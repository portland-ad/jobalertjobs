<?php

namespace App\Controller\Admin;

use App\Entity\JobAlertUser;
use App\Entity\JobCategory;
use App\Form\JobAlertUserType;
use App\Form\JobCategoryType;
use App\Repository\JobCategoryRepository;
use App\Service\JobsService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/jobs-category")
 */
class AdminJobCategoryController extends AbstractFOSRestController
{
    /**
     * @Route("/", methods={"GET"})
     * @param JobsService $jobsService
     * @return Response
     */
    public function index(JobsService $jobsService): Response
    {
        $data = $jobsService->allJobCategoryJson();
        $response = new Response();
        return  $response->setContent(json_encode(['data'=>json_decode($data)]))->setStatusCode(Response::HTTP_OK);

    }

    /**
     * @Route("/new", methods={"GET"})
     */
    public function new(Request $request): Response
    {
        $jobCategory = new JobCategory();
        $form = $this->createForm(JobCategoryType::class, $jobCategory);
        $form->handleRequest($request);

        return $this->render('job_category/new.html.twig', [
            'job_category' => $jobCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", methods={"POST"})
     * @param Request $request
     * @param JobsService $jobsService
     * @return Response
     */
    public function new1(Request $request, JobsService $jobsService):Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $response = new Response();
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $requestData = json_decode($request->getContent(), true);


                $jobCategory = new JobCategory();
                $jobCategory->setName($requestData['name']);
                $jobCategory->setStatus(true);
                $entityManager->persist($jobCategory);
                $entityManager->flush();
                return $this->handleView($this->view(['status'=>'ok', 'job_category' =>$jobCategory],Response::HTTP_CREATED));
            }
            else {
                $jobCategory = new JobCategory();
                $form = $this->createForm(JobCategoryType::class, $jobCategory);
                $form->handleRequest($request);
                $entityManager->persist($jobCategory);
                $entityManager->flush();
                return $this->redirectToRoute('job_category_index');
            }
        }
        catch (\Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'job_category' =>null],500));
        }
    }

    /**
     * @Route("/{id}",name="edit_category",methods={"PUT"})
     * @param Request $request
     * @param String $id
     * @return Response
     */
    public function editJobCategory(Request $request, String $id)
    {
        $em = $this->getDoctrine()->getManager();
        $jobCategoryRepo = $em->getRepository(JobCategory::class);
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $jobCategory = $jobCategoryRepo->find($id);
                $requestData = json_decode($request->getContent(), true);
                $categoryTitle = isset($requestData['name'])?$requestData['name']:$jobCategory->getName();
                $categoryStatus = isset($requestData['status'])?$requestData['status']:$jobCategory->getStatus();

                $jobCategory->setName($categoryTitle);
                $jobCategory->setStatus($categoryStatus);
                $em->flush();
                return $this->handleView($this->view(['status'=>'ok', 'job_category' =>$jobCategory],206));
            }
            return $this->handleView($this->view(['status'=>'error'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
        catch (\Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'job_category' =>null],500));
        }
    }





}
