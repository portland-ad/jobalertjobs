<?php


namespace App\Controller\Admin;


use App\Entity\JobCategory;
use App\Entity\Label;
use App\Form\JobCategoryType;
use App\Form\LabelType;
use App\Repository\LabelRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin/jobs-label")
 */
class LabelController extends AbstractFOSRestController
{
    /**
     * @Route("/", methods={"GET"})
     */
    public function index(LabelRepository $labelRepository): Response
    {
        return $this->render('label/index.html.twig', [
            'labels' => $labelRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new",  methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        try {

            if (strpos($request->headers->get('content-type'), "application/json") > -1) {
                $requestData = json_decode($request->getContent(), true);

                $label = new Label();
                $label->setName($requestData['name']);
                $label->setIsActive(true);
                $label->setCreateAt(new \DateTime());
                $entityManager->persist($label);
                $entityManager->flush();
                return $this->handleView($this->view(['status' => 'ok', 'label' => $label], Response::HTTP_CREATED));
            }

            return $this->handleView($this->view(['status' => 'error', 'label' => null], Response::HTTP_UNSUPPORTED_MEDIA_TYPE));

        } catch (\Exception $e) {
            return $this->handleView($this->view(['status' => 'error', 'label' => null, 'message' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("/{id}",name="edit_label",methods={"PUT"})
     * @param Request $request
     * @param String $id
     * @return Response
     */
    public function editJobType(Request $request, String $id)
    {
        $em = $this->getDoctrine()->getManager();
        $jobLabelRepo = $em->getRepository(Label::class);
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $jobLable = $jobLabelRepo->find($id);
                $requestData = json_decode($request->getContent(), true);
                $name = isset($requestData['name'])?$requestData['name']:$jobLable->getName();
                $isActive = isset($requestData['is_active'])?$requestData['is_active']:$jobLable->getIsActive();
                $jobLable->setName($name);
                $jobLable->setIsActive($isActive);
                $em->flush();
                return $this->handleView($this->view(['status'=>'ok', 'job_role' =>$jobLable],206));
            }
            return $this->handleView($this->view(['status'=>'error'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
        catch (\Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'job_category' =>null],500));
        }
    }


}
