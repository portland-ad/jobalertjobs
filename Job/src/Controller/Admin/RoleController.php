<?php


namespace App\Controller\Admin;

use App\Entity\Label;
use App\Entity\Role;
use App\Form\RoleType;
use App\Repository\RoleRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin/jobs-role")
 */
class RoleController extends AbstractFOSRestController
{
    /**
     * @Route("/", name="role_index", methods={"GET"})
     */
    public function index(RoleRepository $roleRepository): Response
    {
        return $this->render('role/index.html.twig', [
            'roles' => $roleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new",  methods={"POST"})
     */
    public function new(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        try{

            if(strpos($request->headers->get('content-type'),"application/json") > -1) {
                $requestData = json_decode($request->getContent(), true);

                $role = new Role();
                $role->setName($requestData['name']);
                $role->setIsActive(true);
                $role->setCreateAt(new \DateTime());
                $entityManager->persist($role);
                $entityManager->flush();
                return $this->handleView($this->view(['status' => 'ok', 'role' => $role], Response::HTTP_CREATED));
            }

            return $this->handleView($this->view(['status' => 'error', 'role' => null], Response::HTTP_UNSUPPORTED_MEDIA_TYPE));

        }
        catch (\Exception $e)
        {
            return $this->handleView($this->view(['status' => 'error', 'role' => null,'message'=> $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("/{id}",name="edit_role",methods={"PUT"})
     * @param Request $request
     * @param String $id
     * @return Response
     */
    public function editJobType(Request $request, String $id)
    {
        $em = $this->getDoctrine()->getManager();
        $jobRoleRepo = $em->getRepository(Role::class);
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $jobRole = $jobRoleRepo->find($id);
                $requestData = json_decode($request->getContent(), true);
                $name = isset($requestData['name'])?$requestData['name']:$jobRole->getName();
                $isActive = isset($requestData['is_active'])?$requestData['is_active']:$jobRole->getIsActive();
                $jobRole->setName($name);
                $jobRole->setIsActive($isActive);
                $em->flush();
                return $this->handleView($this->view(['status'=>'ok', 'job_role' =>$jobRole],206));
            }
            return $this->handleView($this->view(['status'=>'error'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
        catch (\Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'job_category' =>null],500));
        }
    }


}
