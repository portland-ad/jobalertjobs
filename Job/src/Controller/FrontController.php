<?php

namespace App\Controller;


use App\Entity\ActiveJobEducationReq;
use App\Service\JobsService;
use DateTime;

use App\Entity\City;
use App\Entity\Keyword;
use App\Entity\SearchHistory;
use App\Entity\Jobs;
use App\Entity\ActiveJobs;
use App\Entity\JobAppliers;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use App\Repository\Elasticsearch;
use FOS\ElasticaBundle\Manager\RepositoryManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;
use Symfony\Component\Intl\Currencies;


class FrontController extends AbstractFOSRestController
{
	
	const CACHE_TIME_FOR_KEYWORD_CITY_IN_SECONDS = 150 ;
	const GATEWAY_URL = 'http://13.58.205.236:8080';
	const GATEWAY_ADMIN_URL = 'http://13.58.205.236:9876';
	const GATEWAY_ADMIN_API_KEY = '';
	const EMAIL_SERVICE_API_KEY = '';
	
	protected function __getEM(){
		return $this->getDoctrine()->getManager();
	}
	protected function __getRepository($name){
		
		return $this->__getEM()->getRepository($name);
	}
    /**
     * @Route("/", name="front")
     */
    public function indexAction(Request $request)
    {
        
		$cache = new FilesystemAdapter();
		
		$ip = $request->query->get('ip',$request->getClientIp());
		
		$cRepo = $this->__getRepository(City::class);
		$kRepo = $this->__getRepository(Keyword::class);
		$shRepo = $this->__getRepository(SearchHistory::class);
		
		$q = $request->query->get('q',false);
		$csz = $request->query->get('csz',false);
		
		if($q){
			$dQ = $kRepo->searchAndMapByIdKeyword($q);
			
			if(!empty($dQ)){
				$cacheKeyword = $cache->getItem('keyword');
				if(!$cacheKeyword->isHit()){
					$cacheKeyword->expiresAfter(self::CACHE_TIME_FOR_KEYWORD_CITY_IN_SECONDS);
					$cacheKeyword->set($dQ);
					$cache->save($cacheKeyword);
				}
				else{
					$temp = $cacheKeyword->get();
					$cacheKeyword->expiresAfter(self::CACHE_TIME_FOR_KEYWORD_CITY_IN_SECONDS);
					$cacheKeyword->set(array_merge($temp,$dQ));
					$cache->save($cacheKeyword);
				}
			}
		}
		if($csz){
			$dCSZ = $cRepo->searchAndMapByIdName($csz);
			
			if(!empty($dCSZ)){
				$cacheCSZ = $cache->getItem('city');
				if(!$cacheCSZ->isHit()){
					$cacheCSZ->expiresAfter(self::CACHE_TIME_FOR_KEYWORD_CITY_IN_SECONDS);
					$cacheCSZ->set($dCSZ);
					$cache->save($cacheCSZ);
				}
				else{
					$temp = $cacheCSZ->get();
					$cacheCSZ->expiresAfter(self::CACHE_TIME_FOR_KEYWORD_CITY_IN_SECONDS);
					$cacheCSZ->set(array_merge($temp,$dCSZ));
					$cache->save($cacheCSZ);
				}
			}
		}
		
		if(isset($dQ) || isset($dCSZ)){
			if(empty($dQ) && empty($dCSZ)){
				return $this->json(['message'=>"Not found"],404);
			}
			return $this->json(['keyword'=>$dQ,'city'=>$dCSZ],200);
		}
		
		$return = [] ;
		
		$cacheKeyword = $cache->getItem('keyword');
		if(!$cacheKeyword->isHit()){
			$dK = $kRepo->mapWithLimit('id','keyword',[0,5]);
			if(!empty($dK)){
				$cacheKeyword->set($dK);
				$cacheKeyword->expiresAfter(self::CACHE_TIME_FOR_KEYWORD_CITY_IN_SECONDS);
				$cache->save($cacheKeyword);
				$return ['keyword'] = $dK;
			}
		}
		else{
			$return['keyword'] = $cacheKeyword->get(); 
		}
		
		$cacheCity = $cache->getItem('city');
		if(!$cacheCity->isHit()){
			$dC = $cRepo->mapWithLimit('id','name',[0,5]);
			if(!empty($dC)){
				$cacheCity->set($dC);
				$cacheCity->expiresAfter(self::CACHE_TIME_FOR_KEYWORD_CITY_IN_SECONDS);
				$cache->save($cacheCity);
				$return ['city'] = $dC;
			}
		}
		else{
			$return['city'] = $cacheCity->get(); 
		}
		
		$cacheSearchHistory = $cache->getItem($ip);
		if(!$cacheSearchHistory->isHit()){
			$dSH = $shRepo->mapWithLimit('id','query',[0,5],['ip'=>$ip,'status'=>1]);
			if(!empty($dSH)){
				$cacheSearchHistory->set($dSH);
				$cacheSearchHistory->expiresAfter(self::CACHE_TIME_FOR_KEYWORD_CITY_IN_SECONDS);
				$cache->save($cacheSearchHistory);
				$return ['search_history'] = $dSH;
			}
		}
		else{
			$return['search_history'] = $cacheSearchHistory->get(); 
		}
		
		return $this->json($return,200);
		
    }
    /**
     * @Route("/search",methods={"GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
	//public function getJobsSearchAction(Request $request, RepositoryManagerInterface $finder){
	public function getJobsSearchAction(Request $request){
		
		ini_set('memory_limit', '-1');
		$cache = new FilesystemAdapter();
		
		$page = $request->query->getInt('page',1);
		$limit = $request->query->getInt('limit',10);
		$companyName = $request->query->get('company_name',false);
        $activeJobRepo = $this->__getRepository(ActiveJobs::class);

		if($companyName){
			$activeJobs = $this->__getRepository(ActiveJobs::class)->searchByCompanyName($companyName,$page,$limit);
			if(!$activeJobs){
				return $this->json(['message'=>'No jobs found'],404);
			}


            $searchJob = $activeJobs->getItems();

            $sjb = array();
            foreach ($searchJob as $sj) {
                $ed = $activeJobRepo->findOneBy(['jobId' => $sj['id']])->getJobEducationReq();
                $sj['job_education_req'] = $ed;
                $sjb[] = $sj;
            }

            $activeJobs->setItems($sjb);



			return $this->handleView($this->view($activeJobs,200));
		}
		$q = $request->query->get('q',false);
//		$csz = $request->query->get('csz',false);
		$ip = $request->query->get('ip',$request->getClientIp());
		
		
		$page = empty($page)? 1:($page == ''?1:$page);
		
		// build the cache key
		$cacheKey = $q ?($q!=''?$q:''):'';
//		$cacheKey .= $csz ?($csz!=''?$csz:''):'';
//		$cacheKey .= $csz ? '.'.$page:$page;
		
		if($cacheKey == ''){
			return $this->json(['message'=>'Please search with anything'],404);
		}
		
		// save the search history of this user
		if(!empty($q)){
			$SearchHistory = new SearchHistory();
			$SearchHistory->setQuery($q);
			$SearchHistory->setIp($ip);
			$SearchHistory->setIpAddress($ip);
			$SearchHistory->setStatus(1);
			$SearchHistory->setCreatedAt(new DateTime());
			$SearchHistory->setUpdatedAt(new DateTime());
			
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($SearchHistory);
			$em->flush();
		}
		$return = [] ;
		
		// save search history in cache
		$searchCacheItem = $cache->getItem($ip);
		if($searchCacheItem->isHit()){
			$searchCacheItem->expiresAfter(150);
			$searchCacheItem->set(array_merge($searchCacheItem->get(),[$q]));
			$cache->save($searchCacheItem);
			
			$return ['search_history'] = $searchCacheItem->get() ;
		}
		else{
			$searchCacheItem->expiresAfter(150);
			$searchQuery = [$q] ;
			$searchCacheItem->set($searchQuery);
			$cache->save($searchCacheItem);
			
			$return ['search_history'] = $searchCacheItem->get() ;
		}
		
		
			// Use Elasticsearch to find the result
            //$JobRepo = $finder->getRepository(Jobs::class);
			//$searchResult = $JobRepo->search($q,$page,$limit);

			$searchResult = $activeJobRepo->search($q, $page, $limit);

			$searchJob = $searchResult->getItems();

			$sjb = array();
			$activeJobEducationalRepo = $this->__getRepository(ActiveJobEducationReq::class);
			foreach ($searchJob as $sj) {
                $ed = $activeJobRepo->findOneBy(['jobId' => $sj['id']])->getJobEducationReq();
                $sj['job_education_req'] = $ed;
                $sjb[] = $sj;
            }

        $searchResult->setItems($sjb);

			 $return ['jobs'] = $searchResult;
			if(!count($searchResult)) return $this->handleView($this->view($return,404));
			$return ['jobs'] = $searchResult ;
			return $this->handleView($this->view($return,200));
		
	}
		/**
		@Route("/job-search",methods={"GET"})
	*/
	//public function getJobsSearchAction(Request $request, RepositoryManagerInterface $finder){
	public function getAuthJobsSearchAction(Request $request){
		
		ini_set('memory_limit', '-1');
		
		$userId = $request->headers->get('ja-user-id');
		
		$cache = new FilesystemAdapter();
		
		$page = $request->query->getInt('page',1);
		$limit = $request->query->getInt('limit',10);
		$companyName = $request->query->get('company_name',false);
		
		if($companyName){
			$activeJobs = $this->__getRepository(ActiveJobs::class)->searchByCompanyName($companyName,$page,$limit);
			if(!$activeJobs){
				return $this->json(['message'=>'No jobs found'],404);
			}
			$JobAppliersRepo = $this->__getRepository(JobAppliers::class);
			$searchResult = $JobAppliersRepo->isThisUserApply($activeJobs, $userId);
			return $this->handleView($this->view($searchResult,200));
		}
		$q = $request->query->get('q',false);
//		$csz = $request->query->get('csz',false);
		$ip = $request->query->get('ip',$request->getClientIp());
		
		
		$page = empty($page)? 1:($page == ''?1:$page);
		
		// build the cache key
		$cacheKey = $q ?($q!=''?$q:''):'';
//		$cacheKey .= $csz ?($csz!=''?$csz:''):'';
//		$cacheKey .= $csz ? '.'.$page:$page;
		$cacheKey .= '.'.$userId;
		
		if($cacheKey == ''){
			return $this->json(['message'=>'Please search with anything'],404);
		}
		
		// save the search history of this user
		if(!empty($q)){
			$SearchHistory = new SearchHistory();
			$SearchHistory->setQuery($q);
			$SearchHistory->setIp($ip);
			$SearchHistory->setIpAddress($ip);
			$SearchHistory->setStatus(1);
			$SearchHistory->setUserId($userId);
			$SearchHistory->setCreatedAt(new DateTime());
			$SearchHistory->setUpdatedAt(new DateTime());
			
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($SearchHistory);
			$em->flush();
		}
		$return = [] ;
		
		// save search history in cache
		$searchCacheItem = $cache->getItem($ip);
		if($searchCacheItem->isHit()){
			$searchCacheItem->expiresAfter(150);
//			$searchCacheItem->set(array_merge($searchCacheItem->get(),[$q. ($csz ?' in '.$csz:'')]));
			$cache->save($searchCacheItem);
			
			$return ['search_history'] = $searchCacheItem->get() ;
		}
		else{
			$searchCacheItem->expiresAfter(150);
			$searchQuery = [$q] ;
			$searchCacheItem->set($searchQuery);
			$cache->save($searchCacheItem);
			
			$return ['search_history'] = $searchCacheItem->get() ;
		}
		
			// Use Elasticsearch to find the result
            //$JobRepo = $finder->getRepository(Jobs::class);
			//$searchResult = $JobRepo->search($q,$csz,$page,$limit);
            $JobRepo = $this->__getRepository(ActiveJobs::class);
			$JobAppliersRepo = $this->__getRepository(JobAppliers::class);
			
			$searchResult = $JobRepo->search($q, $page, $limit);
			
			if(empty($searchResult)) return $this->json($return,404);
			//print_r($searchResult);die;
			$searchResult = $JobAppliersRepo->isThisUserApply($searchResult, $userId);
			$return ['jobs'] = $searchResult ;
        return $this->handleView($this->view( $return,200 ));
			
	}
	/**
		@Route("/load-keyword-city")
	*/
	public function loadAction(){
		
		set_time_limit(0);
		
		for($i = 0 ; $i<100; $i++){
			$c = new City();
			$k = new Keyword();
			$em = $this->getDoctrine()->getManager();
		
			$c->setName('city'.$i);
			$c->setStatus(1);
			$c->setCreatedAt(new DateTime());
			$c->setUpdatedAt(new DateTime());
			
			
			$k->setKeyword('keyword'.$i);
			$k->setStatus(1);
			$k->setCreatedAt(new DateTime());
			$k->setUpdatedAt(new DateTime());
			
			$em->persist($c);
			$em->flush();
			
			$em->persist($k);
			$em->flush();
			
		}
		return $this->json(['message'=>'Complete'],201);
	}
	/**
		@Route("/load-jobs")
	*/
	public function loadJobsAction(){
		
		set_time_limit(0);
		$d = $s = $city = $z = [] ;
		$d = [new DateTime(date('2020-05-10 00:00:00')),new DateTime(date('2020-05-11 00:00:00')),new DateTime(date('2020-06-12 00:00:00')),new DateTime(date('2020-06-13 00:00:00')),new DateTime(date('2020-06-14 00:00:00')),new DateTime(date('2020-06-15 00:00:00')),new DateTime(date('2020-06-16 00:00:00')),new DateTime(date('2020-06-17 00:00:00')),new DateTime(date('2020-06-18 00:00:00')),new DateTime(date('2020-06-19 00:00:00')),new DateTime(date('2020-06-20 00:00:00'))];
		$s = ['ss','dd','ee','ff','gg','yy','uu','oo','kl','po'];
		$city = ['ss','dd','ee','ff','gg','yy','uu','oo','kl','po'];
		$z = ['117','211','311','411','511','611','711','811','911','1010'];
		$t = ['','Part Time','Full Time','Contractual'];
		$minSR = [20000,35000,25000,40000,50000,65000,100000,200000,45000,55000] ;
		$maxSR = [20000,45000,25000,40000,50000,65000,100000,210000,45000,55000] ;
		$country = ['BD','CA','USA','CA','EU','CH','IND','IND','CO','UK'];
		$categoryName = [] ;
		$eL = ['','Mid','Entry','TOP'] ;
		$title = ['PHP Developer','React','Front End Developer','Java Developer','Contact Manager','Team Lead','Software Engineer','Production Manager','Sr. Software Engineer','Angular/React/Vue Developer'] ;
		
		
		for($i = 0,$k = 1 ; $i<10; $i++){
			$c = new Jobs();
			
			$em = $this->getDoctrine()->getManager();
		
			$c->setJobTitle($title[$i]);
			$c->setJobDescription('Blah description'.$i);
			$c->setJobResponsiblities('Blah responsibilities'.$i);
			$c->setJobStatus(1);
		
			$c->setLiveAt(new DateTime());
			$c->setEndAt($d[$i]);
			$c->setCreateAt(new DateTime());
			$c->setUpdateAt(new DateTime());
			$c->setCompanyId(1);
			$c->setCompanyName('Digonto IT Services');
			$c->setJobLocation("USA Calfornia");
			$c->setSkillQualificationRequirement("PHP, Software Developments, AJAX, Node Js");
			$c->setEducation("Bsc or Msc In CS/CSE");
			$c->setBenefitFacilities("Share 2% of profit");
			$c->setExperienceId($k);
			$c->setExperienceLevel($eL[$k]);
			$c->setSalaryRange(0);
			$c->setCategoryId(1);
			$c->setCategoryName("Software");
			$c->setZipCode($z[$i]);
			$c->setCity($city[$i]);
			$c->setCountry($country[$i]);
			$c->setSource(1);
			$c->setMinSalaryRange($minSR[$i]);
			$c->setMaxSalaryRange($maxSR[$i]);
			$c->setType($k);
			$c->setTypeInText($t[$k]);
			$c->setState($s[$i]);
			$c->setPriority(0);
			$c->setApplyToEmail('kzrajon@gmail.com');
			$c->setApplyInstruction('Email subject should be EC2500 PHP Developer');
			$c->setGeneralApply(1);
			$c->setApplied(0);
			
			if($i % 3 == 0)$k = 1 ;
			else $k++ ;
			
			

			
			$em->persist($c);
			$em->flush();
			
			
			
		}
		return $this->json(['message'=>'Complete'],201);
	}
	/**
		@Route("/jobs/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/cache-update",methods={"GET"})
	*/
	public function getCacheUpdateAction(Request $request, RepositoryManagerInterface $finder, $id){
		
		$cache = new FilesystemAdapter();
		
		$jobsRepo = $this->__getRepository(Jobs::class);
		
		$data = $jobsRepo->findOneBy(['id'=>$id]);
		
		if(!$data){
			return $this->json(['message'=>'Invalid Jobs Id'],404);
		}
		
		$cacheKey = $data->getJobTitle().'1';  // title+page
		
		$cacheItem = $cache->getItem($cacheKey);
		
		if($cacheItem->isHit()){
			// check the application deadline
			$d = $cacheItem->get();
			$seconds = ($d[0]->getEndAt())->getTimestamp() - ($data->getEndAt())->getTimestamp() ;
			
			if($seconds < 0){
				$cacheItem->set(array_merge($d,[$data]));
			}
			else{
				$cacheItem->set(array_merge([$data],$d));
			}
			$cache->save($cacheItem);
		}
		else{
			$searchResult = $finder->getRepository(Jobs::class)->search($data->getJobTitle());
			if(!empty($searchResult)){
				$cacheItem->expiresAfter($searchResult['cache_time']);
				$cacheItem->set($searchResult);
				$cache->save($cacheItem);
			}
		}
		return $this->json([],200);
	}
	/**
		@Route("/jobs/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/apply",methods={"POST"})
	*/
	public function postJobApplyAction(Request $request, $id){
		
	
		if(strpos($request->headers->get('content-type'),"application/json") > -1){
			$contents = json_decode($request->getContent(),true);
		}
		$userId = $request->headers->get('ja-user-id');
        $consumerId = $request->headers->get('ja-consumer-id');
		$subscriptionId = $request->headers->get('ja-user-subscription-id');
		// let's check the resume status with job source
		$jobData = $this->__getRepository(ActiveJobs::class)->findOneBy(['jobId'=>$id]);
		
		// check if job already exists or not
		if(!$jobData){
			return $this->json(['error'=>'Job not found. Invalid job ID'],404);
		}
		
		// check if user already applied or not
		$JobAppliersRepo = $this->__getRepository(JobAppliers::class);
		if($JobAppliersRepo->alreadyApplied($id,$userId)){
			return $this->json(['error'=>'Already Applied'],409);
		}
		// get the access token by consumerId
		$client = HttpClient::create();
		
		$gatewayData = $client->request('GET',self::GATEWAY_ADMIN_URL.'/tokens/'.$consumerId,[
			'headers'=>[
				'Authorization'=>'apikey '.self::GATEWAY_ADMIN_API_KEY
			]
		]);
		
		$status = $gatewayData->getStatusCode();
		
		if($status != 200){
			return $this->json(['error'=>'Error on tokens API in admin gateway'],500);
		}
		$tokens = $gatewayData->toArray();
		
		
		$resumeData = $client->request('GET',self::GATEWAY_URL.'/resume/status/1',[
			'headers'=>[
				'Authorization'=>'Bearer '.$tokens['access_token'],
				'Content-Type'=>'application/json'
			]
		]);
		$resumeStatus = $resumeData->getStatusCode();
		
		if($resumeStatus == 404){
			return $this->json(['error'=>'Resume not found'],404);
		} 
		if($resumeStatus == 206){
			return $this->json($resumeData->toArray(),206);
		}
		
		if($resumeStatus == 200){
			/*$userData = array_merge($resumeData->toArray(),$contents);
			$emailData = $client->request('POST',self::GATEWAY_URL.'/emails/v1/job-apply',[
				'headers'=>[
					'Authorization'=>'apikey '.self::EMAIL_SERVICE_API_KEY
				],
				'json'=>$userData
			]);
			
			$emailStatus = $emailData->getStatusCode();
			if($emailStatus != 201){
				return $this->json(['error'=>'Email not send'],$emailStatus);
			}
			*/
			$JobAppliers = new JobAppliers();
			$JobAppliers->setJobId($id);
			$JobAppliers->setUserId($userId);
			$JobAppliers->setExpectedSalary($contents['expected_salary']);
			($subscriptionId != '')?$JobAppliers->setSubscriptionId($subscriptionId):'';
			($subscriptionId != '')?$JobAppliers->setStatus(1):$JobAppliers->setStatus(2);
			$JobAppliers->setCurrencyCode($jobData->getCurrencyCode());
			$JobAppliers->setCreatedAt(new DateTime());
			$JobAppliers->setUpdatedAt(new DateTime());
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($JobAppliers);
			$em->flush();
			
			$resumeJobApplyData = $client->request('POST',self::GATEWAY_URL.'/resume/job-apply/'.$id,[
				'headers'=>[
					'Authorization'=>'Bearer '.$tokens['access_token'],
					'Content-Type'=>'application/json'
				],
				'json'=>['expected_salary'=>$contents['expected_salary'],'currency_code'=>$jobData->getCurrencyCode()]
			]);
			$resumeJobApplyStatus = $resumeJobApplyData->getStatusCode();
		    
			if($resumeJobApplyStatus != 201){
			 // send email to developer that resume service do not save job apply data	
			}
			return $this->json(['status'=>'OK'],201);
		}
		
		return $this->json(['error'=>'Something went wrong!'],500);
		
	}
	/**
		@Route("/jobs/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/{userId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/summary-view",methods={"PUT"})
	*/
	public function putSummaryView(Request $request,$id,$userId){
		$jobAppliers = $this->__getRepository(JobAppliers::class)->findOneBy(['jobId'=>$id,'userId'=>$userId]);
		$jobAppliers->setSummaryView(1);
		$em = $this->__getEM();
		$em->flush();
		return $this->json([],204);
	}
	/**
		@Route("/jobs/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/{userId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/details-view",methods={"PUT"})
	*/
	public function putDetailsView(Request $request,$id,$userId){
		$jobAppliers = $this->__getRepository(JobAppliers::class)->findOneBy(['jobId'=>$id,'userId'=>$userId]);
		$jobAppliers->setDetailsView(1);
		$em = $this->__getEM();
		$em->flush();
		return $this->json([],204);
	}
	/**
		@Route("/jobs/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/{userId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/interview-call",methods={"PUT"})
	*/
	public function putInterViewCall(Request $request,$id,$userId){
		
		$contents = json_decode($request->getContent(),true);
		
		$jobAppliers = $this->__getRepository(JobAppliers::class)->findOneBy(['jobId'=>$id,'userId'=>$userId]);
		
		$jobAppliers->setInterViewCall(1);
		$em = $this->__getEM();
		$em->flush();
		$jobs = $this->__getRepository(Jobs::class)->find($id);
		
		$client = HttpClient::create();
		
		$consumerId = $request->headers->get('ja-consumer-id');
		// get the access token by consumerId
		$gatewayData = $client->request('GET',self::GATEWAY_ADMIN_URL.'/tokens/'.$consumerId,[
			'headers'=>[
				'Authorization'=>'apikey '.self::GATEWAY_ADMIN_API_KEY
			]
		]);
		
		$status = $gatewayData->getStatusCode();
		
		if($status != 200){
			return $this->json(['error'=>'Error on tokens API in admin gateway'],500);
		}
		$tokens = $gatewayData->toArray();
		
		/*$companyEmail = $jobs->getApplyToEmail() ;
		if( $companyEmail == null ){
			
			$companyData = $client->request('GET',self::GATEWAY_URL.'/companies/'.$jobs->getCompanyId().'/email',[
				'headers'=>[
					'Authorization'=>'Bearer '.$tokens['access_token']
				]
			]);
			$companyStatus = $companyData->getStatusCode();
			if($companyStatus != 200){
				// send email to developer for "not working company service where company email not found"
			}
			$CD = $companyData->toArray();
			$companyEmail = $CD['email'];
		}
		*/
		// get Resume by userId
		$resumeData = $client->request('GET',self::GATEWAY_URL.'/resume/'.$userId.'/'.$id.'/pdf',[
			'headers'=>[
				'Authorization'=>'Bearer '.$tokens['access_token'],
				'content-type'=>'application/json'
			]
		]);
		$resumeStatus = $resumeData->getStatusCode();
		if($resumeStatus != 200){
			// send email to developer for "not working resume service"
		}
		$R = $resumeData->toArray();
		$resumeFilePath = $R['file'];
		
		$userData = $client->request('GET',self::GATEWAY_URL.'/users/'.$userId.'/email',[
			'headers'=>[
				'Authorization'=>'Bearer '.$tokens['access_token']
			]
		]);
		$userStatus = $userData->toArray();
		if($userStatus != 200){
			// send email to developer that user service not working 
		}
		$U = $userData->toArray();
		$userEmail = $U['email'];
		
		$emailData = $client->request('POST',self::GATEWAY_URL.'/emails/v1/job-apply',[
				'headers'=>[
					'Authorization'=>'apikey '.self::EMAIL_SERVICE_API_KEY
				],
				'json'=>['job_title'=>$jobs->getJobTitle(),'company_name'=>$jobs->getCompanyName(),'company_email'=>$contents['company_email'],'candidate_name'=>$R['name'],'candidate_email'=>$userEmail,'resume_file_path'=>$resumeFilePath,'interview_message'=>$contents['interview_message'],'access_token'=>$tokens['access_token']]
			]);
			
		$emailStatus = $emailData->getStatusCode();
		if($emailStatus != 201){
			return $this->json(['error'=>'Email not send'],$emailStatus);
		}
		
		if($jobAppliers->getSubscriptionId() != null){
			$subscriptionData = $client->request('GET',self::GATEWAY_URL.'/subscriptions/'.$jobAppliers->getSubscriptionId().'/expire-with-inteview-call',[
				'headers'=>[
					'Authorization'=>'Bearer '.$tokens['access_token']
				]
			]);
			$subscriptionStatus = $subscriptionData->getStatusCode();
			if($subscriptionStatus != 200){
				//send email to developers that subscription service not working
			}
			$SD = $subscriptionData->toArray();
			if($SD['expire_with_inteview_call'] != 0){
				$jobAppliers2 = $this->__getRepository(JobAppliers::class)->findBy(['userId'=>$userId,'subscriptionId'=>$jobAppliers->getSubscriptionId(),'status'=>1,'interViewCall'=>1]);
				$totalInterviewCall = count($jobAppliers2);
				if($totalInterviewCall == $SD['expire_with_interview_call']){
					// update status of jobAppliers2 that subscription is expired
					foreach($jobAppliers2 as $appliers){
						$appliers->setStatus(2);
					}
					$em->flush();
					// now unsubscribe this candidate
					$userData = $client->request('PUT',self::GATEWAY_URL.'/users/'.$userId.'/unsubscribe',[
						'headers'=>[
							'Authorization'=>'Bearer '.$tokens['access_token']
						]
					]);
					$userStatus = $userData->getStatusCode();
					if($userStatus != 204){
						
						// send email to developers that unsubcribe not working in user service
					}
				}
			}
		}
		return $this->json([],204);
	}

    /**
     * @Route("/jobs/applied")
     * @param Request $request
     * @param JobsService $jobsService
     * @return Response
     */
	public function getAllAppliedJob(Request $request, JobsService $jobsService):Response
    {
        try{
            $userId = $request->headers->get('ja-user-id');
            $page = $request->query->get('page',1);
            $limit = $request->query->get('limit',10);
			$fromDate = $request->query->get('from_date',null);
			$toDate = $request->query->get('to_date',null);
			$companyName = $request->query->get('company_name',null);
//            $userId = "1";
            if(!$userId) throw new \Exception('User not exited');
            $jobs = $jobsService->getAllAppliedJob($userId,$page,$limit,$fromDate,$toDate,$companyName);
			
            return $this->handleView($this->view(['jobs'=>$jobs,'status'=>'ok'],Response::HTTP_OK));
        } catch (\Exception $exception)
        {
            return $this->json(['status'=>'error','message'=>$exception->getMessage()],Response::HTTP_NOT_FOUND);
        }

    }
    /**
        @Route("job-currency-code",methods={"GET"})
     */
    public function getAllCurrencyCode(Request $request){
        \Locale::setDefault('en');
         $map = $request->query->get('map',false);

        $currencies = Currencies::getNames();

         if($map){
             $tempMap = explode(',',$map);
         }

        return $this->handleView($this->view($currencies,200));
    }
    /**
    @Route("job-currency-code/{currencyCode}",methods={"GET"})
     */
    public function getCurrencyFullNameByCurrencyCode(Request $request, $currencyCode){
        \Locale::setDefault('en');

        $currency = Currencies::getName($currencyCode);
        return $this->handleView($this->view( $currency ,200));
    }
    /**
    @Route("job-currency-code/{currencyCode}/symbol",methods={"GET"})
     */
    public function getCurrencySymbolByCurrencyCode(Request $request, $currencyCode){
        \Locale::setDefault('en');

        $symbol = Currencies::getSymbol($currencyCode);
        return $this->handleView($this->view( $symbol ,200));
    }
	/**
    @Route("remove-expired-jobs",methods={"GET"})
     */
    public function getRemoveExpiredJobs(Request $request){
        
        $dateTime = new DateTime();
		$activeJobRepository = $this->__getRepository(ActiveJobs::class);
		$JobRepository = $this->__getRepository(Jobs::class);
		$totalExpiredJobs = $activeJobRepository->countExpiredJobs($dateTime);
		
		// each time we iterate 500 iteams
		if($totalExpiredJobs <= 10){
			$aJobs = $activeJobRepository->getExpiredJob($dateTime,0,10);
			foreach($aJobs as $job){
				$activeJobRepository->remove($job);
				$J = $JobRepository->find($job->getJobId());
				$J->setJobStatus(0);
				$J->setIsLived(0);
				$J->setIsExpired(1);
				$this->__getEM()->flush();
			}
		}
		else{
			$T = ceil($totalExpiredJobs / 10);
			for($i = 0 ; $i < $T; $i++){
				$aJobs = $activeJobRepository->getExpiredJob($dateTime,($i*10),10);
				foreach($aJobs as $job){
					$activeJobRepository->remove($job);
					$J = $JobRepository->find($job->getJobId());
					$J->setJobStatus(0);
					$J->setIsLived(0);
					$J->setIsExpired(1);
					$this->__getEM()->flush();
				}
			}
		}
        return $this->json([],204);
    }
	
}
