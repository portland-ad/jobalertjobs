<?php

namespace App\Controller;

use App\Domain\JobDomain;
use App\Entity\ActiveJobs;
use App\Entity\City;
use App\Entity\JobAlertUser;
use App\Entity\JobCategory;
use App\Entity\JobEducationReq;
use App\Entity\Jobs;
use App\Entity\Skill;
use App\Entity\JobAppliers;
use App\Form\JobAlertUserType;
use App\Form\JobsType;
use App\Repository\JobsRepository;
use App\Service\JobsService;
use ArrayObject;
use http\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/jobs")
 */
class JobsController extends FOSRestController
{

    const GATEWAY_URL = 'http://13.58.205.236:8080';
    const ADMIN_GATEWAY_URL = 'http://13.58.205.236:9876';
    const ADMIN_GATEWAY_API_KEY = '';


    public function __getTokens($consumerId){
        $client = HttpClient::create();
        $data = $client->request("GET",self::ADMIN_GATEWAY_URL.'/tokens/'.$consumerId,[
            'headers'=>[
                'Authorization'=>'apikey '.self::ADMIN_GATEWAY_API_KEY
            ]
        ]);
        return $data->toArray();
    }
    public function __getComapnyNameAndLogo($id,$request){
        $tokens = $this->__getTokens($request->headers->get('ja-consumer-id'));
        $client = HttpClient::create();
        $data = $client->request("GET",self::GATEWAY_URL.'/companies/'.$id.'/name-logo',[
            'headers'=>[
                'Authorization'=>'Bearer '.$tokens['access_token']
            ]
        ]);
        $d = $data->toArray();
        return $d;
    }
    /**
     * @Route("/", name="jobs_index", methods={"GET"})
     * @param Request $request
     * @param JobsService $jobsService
     * @return Response
     */
    public function index(Request $request, JobsService $jobsService): Response
    {

        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $page = $request->query->get('page',1);
                $limit = $request->query->get('limit',20);
                $userId = $request->headers->get('ja-user-id');
//                $userId = 1;
                $isCompanyType = true|$jobsService->isCompanyType($userId);
                if($isCompanyType)
                {
                    $response = $jobsService->getCompanyJob($userId,$page,$limit);
                    $data['jobs'] = $response->jobs;
                    $data['current_page_number'] = $response->pageNumber;
                    $data['total_count'] = $response->totalCount;
                    $data['num_items_per_page'] = $response->limit;
                    return $this->handleView($this->view(['status'=>'ok','data'=>$data],Response::HTTP_OK));
                }
                else
                {
                    $jobs = $jobsService->getSeekerJobs();
                    $data['jobs'] = $jobs;
                    return $this->handleView($this->view(['status'=>'ok','data'=>$data],Response::HTTP_OK));
                }
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Invalid Header Content Type'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }catch (\Exception $exception) {
            return $this->handleView($this->view(['status'=>'error','message'=>$exception->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("/page/{page}/limit/{limit}")
     * @param Request $request
     * @param int $page
     * @param int $limit
     * @param JobsService $jobsService
     * @return Response
     */
    public function getUserJobsListPaginate(Request $request, int $page=1, int $limit=10, JobsService $jobsService  ):Response
    {
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1) {
                $jobs = $jobsService->getSeekerJobs($page,$limit);
                $data['jobs'] = $jobs;
                return $this->handleView($this->view(['status' => 'ok', 'data' => $data], Response::HTTP_ACCEPTED));
            }
            else
            {
                return $this->handleView($this->view(['status'=>'error','message'=>'Invalid Header Content Type'],Response::HTTP_INTERNAL_SERVER_ERROR));
            }

        }catch (\Exception $exception) {
            return $this->handleView($this->view(['status'=>'error','message'=>$exception->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * @Route("/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}", name="job_detail", methods={"GET"})
     * @param Request $request
     * @param JobsService $jobsService
     * @param string $id
     * @return Response
     */
    public function getJobsDetails(Request $request,JobsService $jobsService, string $id):Response
    {
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1) {
                $jobs = $jobsService->getJobDetails($id);
                $data['jobs'] = $jobs;
                return $this->handleView($this->view(['status' => 'ok', 'data' => $data], Response::HTTP_OK));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Invalid Header Content Type'],Response::HTTP_INTERNAL_SERVER_ERROR));
        }catch (\Exception $exception) {
            return $this->handleView($this->view(['status'=>'error','message'=>$exception->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }


    /**
     * @Route("/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/disable", methods={"PUT"})
     * @param Request $request
     * @param JobsService $jobsService
     * @param string $id
     * @return Response
     */
    public function disableJob(Request $request, JobsService $jobsService, string $id):Response {

        $em = $this->getDoctrine()->getManager();
        try{
            $userId = $request->headers->get('ja-user-id');

            if( !$jobsService->isJobCreateByThisUser($userId, $id))
            {
                return $this->handleView($this->view(['status' => 'error', 'message' => 'job not existed'], Response::HTTP_NOT_FOUND));
            }
            $jobsService->disabledActiveJob($userId,$id);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok', 'message' => 'job disabled successfully'], Response::HTTP_NO_CONTENT));
        } catch (\Exception $exception) {

            return $this->handleView($this->view(['status' => 'error', 'message' => $exception->getMessage(),'errorCause'=> $exception->getTrace()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }


    /**
     * @Route("/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/live", name="make_job_live", methods={"PUT"})
     * @param Request $request
     * @param JobsService $jobsService
     * @param string $id
     * @return Response
     */
    public function makeJobLive(Request $request,JobsService $jobsService, string $id):Response
    {
        $em = $this->getDoctrine()->getManager();
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1) {

                $subscriptionId =  $request->headers->get('ja-user-subscription-id',null);
                $userId =  $request->headers->get('ja-user-id');
				$consumerId = $request->headers->get('ja-consumer-id');
                $jobsRepo = $em->getRepository(Jobs::class);

                $client = HttpClient::create();
                $gatewayData = $client->request('GET', self::ADMIN_GATEWAY_URL . '/tokens/' . $consumerId, [
                    'headers' => [
                        'Authorization' => 'apikey ' . self::ADMIN_GATEWAY_API_KEY
                    ]
                ]);
                $gatewayStatus = $gatewayData->getStatusCode();
                if ($gatewayStatus != 200) {
                   return $this->json(['message'=>'User access token not found from gateway admin side'],500);
                }
                $tokens = $gatewayData->toArray();

				
				
				    $job = $jobsRepo->find($id);
				    if($job->getIsLived() == 1){
                        $jobData = $client->request('PUT', self::GATEWAY_URL . '/jobs/' . $id . '/disable', [
                            'headers' => [
                                'Authorization' => 'Bearer ' . $tokens['access_token']
                            ]
                        ]);
                        $jobStatus = $jobData->getStatusCode();
                        if($jobStatus == Response::HTTP_NO_CONTENT){
                            return $this->json([],Response::HTTP_NO_CONTENT);
                        }
                        else return $this->handleView($this->view($jobData->toArray()));
				        //return $this->json(['message'=>'This Job already Lived'],409);
                    }
                    else{
                        if( is_null($subscriptionId) || $subscriptionId == '' || $job->getBlock() == 1){
                            return $this->json(['message'=>'Resource locked because Subscription expired or this job is blocked'],423);
                        }
                        
                        $subscriptionsData = $client->request('GET',self::GATEWAY_URL.'/subscriptions/'.$subscriptionId.'/total-job-post',[]);
                        $subscriptionsStatus = $subscriptionsData->getStatusCode();
                        if($subscriptionsStatus == 200){
                            $SD = $subscriptionsData->toArray();

                            $totalActiveJobs = $jobsRepo->totalActiveJobsForSubscriptionCheckByUserId($userId);
                            if($SD['total_job_post'] <= $totalActiveJobs){
                                $jobs = $jobsRepo->findBy(['userId'=>$userId,'jobStatus'=>1,'isSubscriptionCheck'=>0]);
                                foreach($jobs as $job){
                                    $job->setIsSubscriptionCheck(1);
                                }
                                $em->flush();
                                if($SD['set_as_expire'] != null || $SD['set_as_expire'] != 0) {
                                    $userData = $client->request('PUT', self::GATEWAY_URL . '/users/' . $userId . '/unsubscribe', [
                                        'headers' => [
                                            'Authorization' => 'Bearer ' . $tokens['access_token']
                                        ]
                                    ]);
                                    return $this->json(['message' => 'Resource locked because Subscription expired'], 423);
                                }
                                else return $this->json(['message'=>'Your Live Job Limit Is Exceed'],423);
                            }
                        }
                }
				
                $tokens = $this->__getTokens($request->headers->get('ja-consumer-id'));
                $jobs = $jobsService->makeJobLive($id, $subscriptionId, $tokens);
                $data['jobs'] = $jobs;
                $em->flush();
                return $this->handleView($this->view(['status' => 'ok', 'message' => 'Job is been lived successfully','activeJob'=>$jobs ], 204));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Invalid Header Content Type'],Response::HTTP_INTERNAL_SERVER_ERROR));
        }catch (\Exception $exception) {
            return $this->handleView($this->view(['status'=>'error','message'=>$exception->getMessage()],$exception->getCode()));
        }
    }
 
    /**
     * @Route("/new", name="job_new_get", methods={"GET"})
     */
    public function createNewjobGet(Request $request, JobsService $jobsService): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        try {
            $jobDomain = new JobDomain();
            $jobDomain->jobCategory = $entityManager->getRepository(JobCategory::class)->findBy(['status'=>true]);
            $jobDomain->jobEducationReq =  $entityManager->getRepository(JobEducationReq::class)->findAll();
            $jobDomain->skills = $entityManager->getRepository(Skill::class)->findAll();

            $jobType = [
                "1"=>"parttime job",
                "2"=>" full time job",
                "3"=>"time flexible",
                "4"=>"remote",
            ];
            $jobDomain->type = $jobType;
            return $this->handleView($this->view(['status'=>'ok', 'data' => $jobDomain],Response::HTTP_OK));


        }
        catch( Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'message' => $e->getMessage()],Response::HTTP_OK));

        }

        return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
    }


    /**
     * @Route("/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}", name="edit_job", methods={"PUT"})
     * @param Request $request
     * @param string $id
     * @param JobsService $jobsService
     * @return Response
     */
    public function editJob(Request $request, string $id, JobsService $jobsService):Response
    {

        $entityManager = $this->getDoctrine()->getManager();
        try {
            if(strpos($request->headers->get('content-type'),"application/json") > -1) {
                $requestData = json_decode($request->getContent(), true);
                $userId = $request->headers->get('ja-user-id');
                $requestData['userId'] = $userId;
                $response = $jobsService->editJob(json_encode($requestData),$id);
                if($response->error)
                {
                    return $this->handleView($this->view(['status'=>'error', 'message' => $response->error],Response::HTTP_INTERNAL_SERVER_ERROR));

                }
                $entityManager->flush();

                return $this->handleView($this->view(['status'=>'ok','message'=>'Job Updated Successfully', 'job'=>$response->job],Response::HTTP_CREATED));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Invalid Header Content Type'],Response::HTTP_INTERNAL_SERVER_ERROR));

        }
        catch( Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'message' => $e->getMessage()],Response::HTTP_CREATED));

        }

        return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
    }

    /**
     * @Route("/", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function createNewJob(Request $request, JobsService $jobsService):Response
    {

        $entityManager = $this->getDoctrine()->getManager();
        try {
            if(strpos($request->headers->get('content-type'),"application/json") > -1) {
                $requestData = json_decode($request->getContent(), true);
                $userId = $request->headers->get('ja-user-id');
                $requestData['user_id'] = $userId;
                $requestData['subscription_id'] = $request->headers->get('ja-user-subscription-id',null);
                $companyNameAndLogo = $this->__getComapnyNameAndLogo($requestData['company_id'],$request);
                $requestData['company_name'] = isset($companyNameAndLogo['company_name'])? $companyNameAndLogo['company_name']:'';
                $requestData['company_logo'] = isset($companyNameAndLogo['company_logo'])?$companyNameAndLogo['company_logo']:'';

                $response = $jobsService->createNewJob(json_encode($requestData));

                if($response->error)
                {
                    return $this->handleView($this->view(['status'=>'error', 'message' => $response->error],Response::HTTP_INTERNAL_SERVER_ERROR));

                }
                $entityManager->persist($response->job);
                $entityManager->flush();

                return $this->handleView($this->view(['status'=>'ok','job'=>$response->job],Response::HTTP_CREATED));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Invalid Header Content Type'],Response::HTTP_INTERNAL_SERVER_ERROR));

        }
        catch( Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'message' => $e->getMessage()],Response::HTTP_CREATED));

        }

        return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
    }

    /**
     * @Route("/drafts-job",methods={"GET"})
     * @param Request $request
     * @param JobsService $jobsService
     * @return Response
     */
    public function viewDraftJobs(Request $request, JobsService $jobsService):Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        try {
			$page = $request->query->get('page',1);
			$limit = $request->query->get('limit',20);
			
            if(strpos($request->headers->get('content-type'),"application/json") > -1) {
                $subscriptionId = $request->headers->get('ja-user-subscription-id');
                $consumerId = $request->headers->get('ja-consumer-id');
                $subscription = $jobsService->getSubscriptionInfo($subscriptionId, $consumerId);
                $userId = $request->headers->get('ja-user-id');
//                $userId = 1;
                $paginator = $jobsService->getDraftJob($userId, $page, $limit);
                $data['job'] = $paginator;
                $data['subscription'] = $subscription;
                return $this->handleView($this->view(['data'=> $data],Response::HTTP_OK));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Invalid Header Content Type'],415));

        }
        catch( Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'message' => $e->getMessage()],Response::HTTP_CREATED));

        }

    }

    /**
     * @Route("/myjobs/page/{page}/limit/{limit}",methods={"GET"})
     * @param Request $request
     * @param JobsService $jobsService
     * @param string $page
     * @param string $limit
     * @return Response
     */
    public function myJobList(Request $request, JobsService $jobsService, string $page = "1", string $limit = "10"):Response{

        $entityManager = $this->getDoctrine()->getManager();
        try {
            if(strpos($request->headers->get('content-type'),"application/json") > -1) {

                $userId = $request->headers->get('ja-user-id');

                $paginator = $jobsService->getMyJobs($userId, $page, $limit);
                $data['job'] = $paginator;
                return $this->handleView($this->view(['data'=> $data],Response::HTTP_OK));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Invalid Header Content Type'],Response::HTTP_INTERNAL_SERVER_ERROR));

        }
        catch( Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'message' => $e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));

        }

    }

    /**
     * @Route("/new", name="jobs_create", methods={"POST"})
     * @param Request $request
     * @param JobsService $jobsService
     * @return Response
     */
    public function newJobPost(Request $request, JobsService $jobsService): Response
    {
        $response = new Response();
      try {

        $entityManager = $this->getDoctrine()->getManager();
        if(strpos($request->headers->get('content-type'),"application/json") > -1) {
            $requestData = json_decode($request->getContent(), true);
            $data = $jobsService->createNewJob(json_encode($requestData));
            $entityManager->flush();
            return $response->setContent(json_encode([
            'data' => json_decode($data), 'errors' => null, 'message'=> "Job is Posted Successfully"
            ]))->setStatusCode(Response::HTTP_CREATED);

        }

      }
      catch( Exception $e)
      {
        return $response->setContent(json_encode([
            'data' => null, 'errors' => $e->getMessage()
        ]));
      }
    }

    /**
     * @Route("/show/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}", name="jobs_details", methods={"GET"})
     * @param string $id
     * @param JobsService $jobsService
     * @return Response
     */
    public function show(string $id, JobsService $jobsService): Response
    {

// this function route is created for showing detail of job

        $response = new Response();
        try{
            $job = $jobsService->jobDetails($id);
            return $response->setContent(json_encode(['data'=> json_decode($job), 'error'=> null]))->setStatusCode(Response::HTTP_OK);

        } catch (\Exception $e)
        {
            return $response->setContent(json_encode(['data'=> null, 'error'=>$e->getMessage()]))->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/show-more/job-alert/{email}", name="jobs_show-more", methods={"GET"})
     * @param Request $request
     * @param JobsService $jobsService
     * @param string $email
     * @return Response
     */

    public function showMore(Request $request, JobsService $jobsService, string $email): Response
    {

// this function is created for
        $response = new Response();
        try{
            $jobList = $jobsService->seeMoreJob($email);

            $data = array();
            foreach ($jobList as $job)
            {
                array_push($data,$data);

            }
            return $response->setContent(json_encode(['data'=> $job, 'error'=> null]))->setStatusCode(Response::HTTP_OK);

        } catch (\Exception $e)
        {
            return $response->setContent(json_encode(['data'=> serialize($jobList[0]), 'error'=>$e->getMessage()]))->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/edit", name="jobs_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Jobs $job): Response
    {
        $form = $this->createForm(JobsType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('jobs_index');
        }

        return $this->render('jobs/edit.html.twig', [
            'job' => $job,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}", name="jobs_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Jobs $job): Response
    {
        if ($this->isCsrfTokenValid('delete'.$job->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($job);
            $entityManager->flush();
        }

        return $this->redirectToRoute('jobs_index');
    }
	 /**
      @Route("/{companyId}/rate/{rate}",name="update_company_rate",methods={"PUT"})
    */
    public function putCompanyRate(Request $request,$companyId,$rate){
        $em = $this->getDoctrine()->getManager();
        $jobRepo = $em->getRepository(Jobs::class);
        $data = $jobRepo->findBy(['jobStatus'=>1,'companyId'=>$companyId]);
        if(!$data){
          return $this->json(['message'=>'Active job not found'],404);
        }
        foreach($data as $job){
          $job->setRate($rate);
        }
      
        $em->flush();
        return $this->json(['message'=>count($data).' jobs rate updated'],204);
    }

    /**
     * @Route("/draftjob")
     * @param Request $request
     * @param JobsService $jobsService
     * @param string $page
     * @param string $limit
     * @return Response
     */
    public function getDraftJob(Request $request, JobsService $jobsService, string $page = "1", string $limit = "10"):Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        try {
            if(strpos($request->headers->get('content-type'),"application/json") > -1) {

                $userId = $request->headers->get('ja-user-id');
//                $userId = 1;
                $paginator = $jobsService->getMyJobs($userId, $page, $limit);
                $data['job'] = $paginator;
                return $this->handleView($this->view(['data'=> $data],Response::HTTP_OK));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Invalid Header Content Type'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
        catch( Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'message' => $e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));

        }
    }
    /**
     * @Route("/expired-job")
     * @param Request $request
     * @param JobsService $jobsService
     * @param string $page
     * @param string $limit
     * @return Response
     */
    public function getExpiredJob(Request $request, JobsService $jobsService, string $page = "1", string $limit = "10"):Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        try {
            if(strpos($request->headers->get('content-type'),"application/json") > -1) {

                $userId = $request->headers->get('ja-user-id');
//                $userId = 1;
                $paginator = $jobsService->getExpiredJobs($userId, $page, $limit);
                $data['job'] = $paginator;
                return $this->handleView($this->view(['data'=> $data],Response::HTTP_OK));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Invalid Header Content Type'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
        catch( Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error', 'message' => $e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));

        }
    }
    /**
     * @Route("/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/{userId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/shortlist",methods={"PUT"})
     * @param Request $request
     * @param $id
     * @param $userId
     * @param JobsService $jobsService
     */
	public function putJobAppliersShortList(Request $request,$id,$userId, JobsService $jobsService) :Response
    {
        $em = $this->getDoctrine()->getManager();
        try{
            $jobApplier = $em->getRepository(JobAppliers::class)->findOneBy(['jobId'=>$id,'userId'=>$userId]);
            if($jobApplier->getShortListed() == null || $jobApplier->getShortListed() == 0){
                $jobApplier->setShortListed(1);
            }
            else $jobApplier->setShortListed(0);
            $tokens = $this->__getTokens($request->headers->get('ja-consumer-id'))['access_token'];
            if(!$jobsService->updateResumeServiceJobApplier($id, $userId, $tokens))
            {
                throw new \Exception('Resume service update false');
            }

            $em->flush();
            return $this->json(['short_list'=>$jobApplier->getShortListed() == 1?true:false],206);

        } catch (\Exception $exception)
        {
            return $this->handleView($this->view(['status'=>'error', 'message' => $exception->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }

	}
    /**
    @Route("/company-id/{companyId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}",methods={"GET"})
     */
    public function getJobsByCompanyId(Request $request, $companyId){
        $page = $request->query->get('page',1);
        $limit = $request->query->get('limit',10);
        $jobs = $this->getDoctrine()->getRepository(Jobs::class)->findByCompanyId($companyId,$page,$limit);
        if(!$jobs){
            return $this->handleView($this->view(['message'=>'Job not found']),404);
        }
        return $this->handleView($this->view($jobs),200);
    }
    /**
     * @param JobsService $jobsService
     * @route("/expertize-label/")
     */
	public function getEmployeeExpertLabel(JobsService $jobsService)
    {
        $data = $jobsService->getCandidateExperienceLable();
        return $this->handleView($this->view(['data'=> $jobsService->getCandidateExperienceLable()],Response::HTTP_OK));
    }

    /**
     * @Route("/{companyId}/block/{block}}",methods={"PUT"})
     * @param Request $request
     * @param JobsService $jobsService
     * @param $companyId
     * @param $block
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putBlockJobsByCompanyId(Request $request,JobsService $jobsService,$companyId,$block){
        
      $liveJobs = $this->getDoctrine()->getManager()->getRepository(Jobs::class)->findBy(['isLived'=>1,'companyId'=>$companyId]);
      if(count($liveJobs) > 0){
            $request->headers->set('content-type','application/json');
            foreach($liveJobs as $LJ){
                $this->makeJobLive($request,$jobService,$LJ->getId());
            }
      }
      $status = $this->getDoctrine()->getManager()->getRepository(Jobs::class)->blockOrUnblockJobsByCompanyId($companyId,$block);
      return $this->json([],204);
    }


    /**
     * @Route("/company-logo/{companyId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}", methods={"PUT"})
     * @param Request $request
     * @param String $companyId
     * @return Response
     */
    public function updateCompanyLogo(Request $request, String $companyId){
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent());
        try{
            $jobs = $em->getRepository(Jobs::class)->findBy(['companyId'=> $companyId]);
            $activeJobs = $em->getRepository(ActiveJobs::class)->findBy(['companyId'=> $companyId]);
            $logo = $data->logo;
            foreach ($jobs as $job) {
                $job->setCompanyLogo($logo);
            }

            foreach ($activeJobs as $job) {
                $job->setCompanyLogo($logo);
            }


            $em->flush();
            return $this->handleView($this->view(['status'=> 'ok','logo'=> $logo],206));
        } catch (\Exception $ex) {
            return $this->handleView($this->view(['status'=> 'error', 'message'=> $ex->getMessage()],500));
        }
    }










//    /**
//     * @Route("/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/getall/")
//     * @Route("/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/getall/page/{page}")
//     * @Route("/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/getall/limit/{limit}")
//     * @Route("/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/getall/page/{page}/limit/{limit}")
//     * @param Request $request
//     * @param JobsService $jobsService
//     * @param string $id
//     * @param int $page
//     * @param int $limit
//     * @return Response
//     */
//    public function getApplicantList(Request $request, JobsService $jobsService, string $id,int $page = 1, int $limit = 20)
//    {
//
//        $em = $this->getDoctrine()->getManager();
//        try{
//            $userId = $userId = $request->headers->get('ja-user-id');
//            $jobId = $id;
//            $applicantList = $jobsService->getApplicantIdList($userId,$id,$page,$limit);
//
//        } catch (\Exception $exception)
//        {
//
//        }
//    }


}

