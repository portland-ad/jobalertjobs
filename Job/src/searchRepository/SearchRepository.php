<?php

namespace App\searchRepository;

use DateTime;
use FOS\ElasticaBundle\Repository;

use Elastica\Query;
use Elastica\Query\Term;
use Elastica\Query\Terms;
use Elastica\Query\Match;
use Elastica\Query\BoolQuery;
use Elastica\Query\Wildcard;
use Elastica\Query\MatchAll;
use Elastica\Query\QueryString;
use Elastica\Query\MultiMatch;
use Elastica\Query\Range;


class SearchRepository extends Repository{
	
	public function search($q, $csz = null, $page = 1, $limit = 10, $options = ['job_status'=> 1]){
		
		$boolQuery = new BoolQuery();
		
		$qs = new QueryString();
		$qs->setQuery("*".$q."*");
		$qs->setFields([
			'job_title',
			'job_description',
			'job_responsiblities',
			'company_id',
			'company_name',
			'job_location',
			'skill_qualification_requirement',
			'education',
			'benefit_facilities',
			'experience_level',
			'category_name',
			'country',
			'min_salary_range',
			'max_salary_range',
			'type_in_text'
			]);
		$qs->setDefaultOperator('AND');
		//print_r(get_class_methods($qs));
		$boolQuery->addMust($qs);
		
		if(!is_null($csz) && $csz !='' && $csz != false){
			$cszQs = new QueryString();
			
			if(is_numeric($csz)){
				$cszQs->setQuery($csz);
				$cszQs->setFields(['zip_code']);
			}
			else{
				$cszQs->setQuery("*".$csz."*");
				$cszQs->setFields(['city','state']);
				$cszQs->setDefaultOperator('AND');
			}
			$boolQuery->addMust($cszQs);
		}
		
		$statusQ = new Match();
		$statusQ->setParams(['job_status'=>'1']);
		$boolQuery->addMust($statusQ);
		
		
		$liveAtQuery = new Range();
		$liveAtQuery->setParams(['live_at'=>['lte'=>date('Y-m-d H:i:s')]]);
		$boolQuery->addMust($liveAtQuery);
		
		$endAtQuery = new Range('end_at', [
				'gte' => date('Y-m-d')
			]);
		$boolQuery->addMust($endAtQuery);
		
		
		
		$qq = new Query();
		$qq->setQuery($boolQuery);
		$qq->setSort(['end_at'=>['order'=>'asc']]);
		$qq->setFrom(($page-1)*$limit);
		//print_r(json_encode($qq->toArray()));
		$rr = $this->find($qq,$limit);
		
		if(empty($rr)) return $rr ;
		
		return [
			'result'=>$rr,
			'page'=>$page,
			'limit'=>$limit,
		    'cache_time'=>$this->findRecentlyCacheEndedTime($rr[0]->getEndAt())
		];
		
	}
	public function findRecentlyCacheEndedTime($endDate){
		return $endDate->getTimestamp() - (new DateTime())->getTimestamp();
	}
}
?>