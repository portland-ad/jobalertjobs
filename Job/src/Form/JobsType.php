<?php

namespace App\Form;

use App\Entity\Jobs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jobTitle')
            ->add('jobDercription')
            ->add('jobResponsiblities')
            ->add('jobStatus')
            ->add('companyId')
            ->add('jobLocation')
            ->add('skillQualificationRequiment')
            ->add('Education')
            ->add('benefitFacilities')
            ->add('experienceLavel')
            ->add('salaryRange')
            ->add('jobCategory')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Jobs::class,
        ]);
    }
}
