<?php

namespace App\Form;

use App\Entity\JobAlertUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobAlertUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('keywords')
            ->add('fullName')
            ->add('email')
            ->add('NameOfAlertedJob')
            ->add('location')
            ->add('workExperience')
            ->add('expectedSlary')
            ->add('label')
            ->add('industry')
            ->add('role')
            ->add('jobCategory')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => JobAlertUser::class,
        ]);
    }
}
