<?php


namespace App\Domain;


class ExpertizeLabel
{
    private $key;
    private $value;

    function __construct($key, $value) {
        $this->key = $key;
        $this->value = $value;
    }

    public function setKey(string $key):self
    {
        $this->key = $key;
    }

    public function getKey():string
    {
        return $this->key;
    }

    public function setValue(string $value):self
    {
        $this->value = $value;
    }

    public function getValue():string
    {
        return $this->value;
    }
}