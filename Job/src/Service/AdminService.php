<?php


namespace App\Service;


use App\Entity\City;
use App\Entity\JobCategory;
use App\Entity\Keyword;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerBuilder;

class AdminService
{

    private $em;
    private $serializer;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->serializer = SerializerBuilder::create()->build();

    }

    public function  createNewJobCategory($job_category)
    {
        $response = new \stdClass;
        $response->error = null;
        $jobCategoryRipo = $this->em->getRepository(JobCategory::class);



        $job_category = json_decode($job_category);
        try{
            $jobCategory = new JobCategory();
            if (isset($job_category->name)) {
                $existCategory = $jobCategoryRipo->findOneBy(['name'=>$job_category->name]);
                if($existCategory){
                    throw new \Exception("Job Category already exists");
                }
                $jobCategory->setName($job_category->name);
            } else {
                throw new \Exception("Job Description is required");
            }
            $this->em->persist($jobCategory);
            $response->jobCategory = $jobCategory;
            return $response;

        } catch (\Exception $e)
        {
            $response->jobCategory = null;
            $response->error = $e->getMessage();
            return $response;
        }
    }


}