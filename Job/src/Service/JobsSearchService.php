<?php


namespace App\Service;


use App\Entity\City;
use App\Entity\Keyword;
use App\Repository\KeywordRepository;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerBuilder;

class JobsSearchService
{

    private $em;
    private $serializer;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->serializer = SerializerBuilder::create()->build();

    }

    public function findJobSearchKeyWord($keyWord) {
        $requestData = json_decode($keyWord);
        $keyWordRipo = $this->em->getRepository(Keyword::class);
        if(!isset($requestData->keyword))
        {
            throw new \Exception('City name must be provided');
        }
        $likeKeyWordData = $keyWordRipo->findSearchKeyWord($requestData->keyword);
        return $this->serializer->serialize($likeKeyWordData, 'json');
    }

    public function findJobSearchCityName($cityName) {
        $requestData = json_decode($cityName);
        $keyWordRipo = $this->em->getRepository(City::class);
        if(!isset($requestData->city))
        {
            throw new \Exception('City name must be provided');
        }
        $likeKeyCityData = $keyWordRipo->findSearchCityName($requestData->city);
        return $this->serializer->serialize($likeKeyCityData, 'json');
    }


}