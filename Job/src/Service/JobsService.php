<?php

namespace App\Service;

use DateInterval ;
use App\Domain\ExpertizeLabel;
use App\Entity\ActiveJobCategory;
use App\Entity\ActiveJobEducationReq;
use App\Entity\ActiveJobFacility;
use App\Entity\ActiveJobResponsibility;
use App\Entity\ActiveJobs;
use App\Entity\ActiveSkill;
use App\Entity\Experience;
use App\Entity\JobAppliers;
use App\Entity\JobEducationReq;
use App\Entity\JobFacility;
use App\Entity\JobResponsibility;
use App\Entity\JobType;
use App\Entity\Label;
use App\Entity\Role;
use App\Entity\Skill;
use App\Entity\SkillQualificationReq;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Elastica\Processor\Date;
use JMS\Serializer\SerializerBuilder;
use App\Entity\JobCategory;
use App\Entity\Jobs;
use App\Entity\JobAlertUser;
// use Symfony\Component\Validator\Validator\ValidatorInterface;

use DateTime;
use Knp\Component\Pager\PaginatorInterface;
use mysql_xdevapi\Exception;
use PhpParser\Node\Expr\FuncCall;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\Json;

class JobsService {

//	const GATEWAY_URL = 'http://localhost:8080';
    const GATEWAY_URL = 'http://13.58.205.236:8080';
	const EMAIL_SERVICE_API_KEY = '';
	const FRONTEND_URL = '' ;
    const GATEWAY_ADMIN_API_KEY = '';
	
    private $em;
    private $serializer;
    private $httpClient;
    private $router;
    private $paginator;

    public function __construct(EntityManagerInterface $em,UrlGeneratorInterface $router,PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->serializer = SerializerBuilder::create()->build();
        $this->httpClient = HttpClient::create();
        $this->router = $router;
        $this->paginator = $paginator;

    }

    public function makeHttpCall($method, $path, $token ='', $data = [])
    {
        return $this->httpClient->request(strtoupper($method), $path, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => $data
        ]);

    }

    public function allJobCategoryJson()
    {
        $jobRipo = $this->em->getRepository(JobCategory::class);
        $jobs = $jobRipo->findAll();
        $jobJson = $this->serializer->serialize($jobs, 'json');
        return $jobJson;
    }


    public function getAllJobsJson()
    {
        $jobRipo = $this->em->getRepository(Jobs::class);
        $jobs = $jobRipo->findAll();
        $jobJson = $this->serializer->serialize($jobs, 'json');
        return $jobJson;

    }

    public function createOrUpdateJobByJson($jsonJobParameters)
    {


        $jobCategoryRipo = $this->em->getRepository(JobCategory::class);

        $newData = json_decode($jsonJobParameters);

        if(isset($newData->id) && $newData->id != null) {
            $job = $this->em->getRepository(Jobs::class)->find($newData->id);
        }

        $job->setJobTitle($newData->jobTitle);
        $job->setJobDercription($newData->jobDescription);
        $job->setJobStatus($newData->jobStatus);
        $job->setCreateAt(new \DateTime());
        $job->setExperienceLavel($newData->experienceLabel);
        $job->setCompanyId($newData->company_id);
        $job->setJobLocation($newData->jobLocation);
        $job->setJobResponsiblities($newData->jobResponsibilities);
        $job->setSkillQualificationRequiment($newData->skillRequirment);
        $job->setEducation($newData->education);
        $job->setLiveAt(new \DateTime());
        $job->setEndAt(new \DateTime());
        foreach($newData->category as $data)
        {
            $jobCategory = $jobCategoryRipo->find($data);
            $job->addJobCategory($jobCategory);
        }
         if(!isset($newData->id)) {
            $this->em->persist($job);
         }

        $jsonObject = $this->serializer->serialize($job, 'json');
        return $jsonObject;

    }

    public function createNewJob($jobData)
    {
        $response = new \stdClass;
        $response->error = null;

        try {
            $categoryRipo = $this->em->getRepository(JobCategory::class);
            $job = new Jobs();
            $jobData = json_decode($jobData);
            $job->setApplyInstruction($jobData->apply_instruction);
            $job->setCompanyLogo($jobData->company_logo);
            $job->setUserId($jobData->user_id);
            $job->setSubscriptionId($jobData->subscription_id);
            $job->setCurrencyCode($jobData->currency_code);
            if (isset($jobData->job_title)) {
                $job->setJobTitle($jobData->job_title);
            } else {
                throw new \Exception("Job Title is required");
            }

            if (isset($jobData->job_description)) {
                $job->setJobDescription($jobData->job_description);
            } else {
                throw new \Exception("Job Description is required");
            }

            $job->setJobStatus(0);

            if (isset($jobData->type)) {
                $typeInText = $this->em->getRepository(JobType::class)->findOneBy(['type'=>$jobData->type])->getTypeInText();
                $job->setTypeInText($typeInText);
                $job->setType($jobData->type);
            } else {
                throw new \Exception("Job Type is required  is required like part time fulltime");
            }

            $job->setCreateAt(new DateTime());
            //$job->setCompanyId($jobData->company_id);
            if (isset($jobData->company_id)) {
                $job->setCompanyId($jobData->company_id);
            } else {
                throw new \Exception("Company ID is required ");
            }

            if (isset($jobData->company_name)) {
                $job->setCompanyName($jobData->company_name);
            } else {
                throw new \Exception("Company Name is required ");
            }

            if (isset($jobData->job_location)) {
                $job->setJobLocation($jobData->job_location);
            } else {
                throw new \Exception("Job Location  is required");
            }

            if (isset($jobData->country)) {
                $job->setCountry($jobData->country);
            } else {
                throw new \Exception("Country  is required ");
            }

            if (isset($jobData->city)) {
                $job->setCity($jobData->city);
            } else {
                throw new \Exception("City  is required");
            }

            if (isset($jobData->state)) {
                //not requied field
                $job->setState($jobData->state);
            }

            if (isset($jobData->zip_code)) {
                //not requied field
                $job->setZipCode($jobData->zip_code);
            }

            if (isset($jobData->min_salary_range)) {
                $job->setMinSalaryRange($jobData->min_salary_range);
            } else {
                throw new \Exception("Min Salary  is required");
            }

            if (isset($jobData->max_salary_range)) {
                $job->setMaxSalaryRange($jobData->max_salary_range);
            } else {
                throw new \Exception("Max Salary  is required");
            }

            if (isset($jobData->job_category)) {
                foreach ($jobData->job_category as $categoryData) {
                    $category = new JobCategory();
                    if (isset($categoryData->id)) {
                        $category = $categoryRipo->find($categoryData->id);
                    } else {
                        $category->setName($categoryData->name);
                        $category->setStatus(true);
                        $this->em->persist($category);

                    }
                    $job->addJobCategory($category);
                }
            } else {
                throw new \Exception("Job Category is required");
            }


            if (isset($jobData->job_education_req)) {
                $jobEducationalReqRipo = $this->em->getRepository(JobEducationReq::class);
                foreach ($jobData->job_education_req as $educationalReq) {
                    $jobEducationalReq = new JobEducationReq();
                    if (isset($educationalReq->id)) {
                        $jobEducationalReq = $jobEducationalReqRipo->find($educationalReq->id);
                    } else {
                        $jobEducationalReq->setDegreTitle($educationalReq->degre_title);
                        $this->em->persist($jobEducationalReq);
                    }
                    $job->addJobEducationReq($jobEducationalReq);
                }
            } else {
                throw new \Exception("Educational Requirement  is required");
            }

            if (isset($jobData->job_facilities)) {
                $jobFacilitiesRipo = $this->em->getRepository(JobFacility::class);
                foreach ($jobData->job_facilities as $job_facility) {
                    $jobFacility = new JobFacility();
                    if (isset($job_facility->id)) {
                        $jobFacility = $jobFacilitiesRipo->find($job_facility->id);
                    } else {
                        $jobFacility->setText($job_facility->text);
                        $jobFacility->setTitle(isset($job_facility->title)?$job_facility->title:null);
                        $this->em->persist($jobFacility);
                    }
                    $job->addJobFacility($jobFacility);
                }
            } else {
                throw new \Exception("Job Facilities  is required");
            }

            if (isset($jobData->job_responsibilities)) {
                $jobResponsibilityRipo = $this->em->getRepository(JobResponsibility::class);
                foreach ($jobData->job_responsibilities as $job_responsibility) {
                    $jobResponsibility = new JobResponsibility();
                    if (isset($job_responsibility->id)) {
                        $jobResponsibility = $jobResponsibilityRipo->find($job_responsibility->id);
                    } else {
                        $jobResponsibility->setText($job_responsibility->text);

                        if (isset($job_responsibility->is_additional)) {
                            $jobResponsibility->setIsAdditional($job_responsibility->is_additional);
                        } else {
                            $jobResponsibility->setIsAdditional(false);
                        }

                        $this->em->persist($jobResponsibility);
                    }
                    $job->addJobResponsibility($jobResponsibility);
                }
            } else {
                throw new \Exception("Job Facilities  is required");
            }

            if (isset($jobData->skills)) {
                $jobSkillsRipo = $this->em->getRepository(Skill::class);
                foreach ($jobData->skills as $job_skill) {
                    $skill = new Skill();
                    if (isset($job_skill->id)) {

                        $skill = $jobSkillsRipo->find($job_skill->id);
                        if(!$skill)
                        {
                            $skill = new Skill();
                            $skill->setTitle($job_skill->title);
                            $skill->setId($job_skill->id);

                            if(isset($job_skill->created_at))
                            {
                                $skill->setCreatedAt(new \DateTime($job_skill->created_at));
                            }
                            else
                            {
                                $skill->setCreatedAt(new \DateTime());
                            }
                            if(isset($job_skill->update_at))
                            {
                                $skill->setUpdateAt(new \DateTime($job_skill->update_at));
                            }
                            else
                            {
                                $skill->setUpdateAt(new \DateTime());
                            }

                            $this->em->persist($skill);
                        }
                    } else {
                        $skill->setTitle($job_skill->title);
                        $this->em->persist($skill);
                    }
                    $job->addSkill($skill);
                }
            } else {
                throw new \Exception("Job Skills  is required");
            }

            $response->job = $job;
            return $response;
        }
        catch (\Exception $e)
        {
            $response->job = null;
            $response->error = $e->getMessage();
            return $response;

        }

    }

    public function createOrUpdateJobByHtml($job)
    {
        // need to work on here
         
        // need to work here
    
        $job->setLiveAt(new \DateTime());
        $job->setEndAt(new \DateTime());
        $job->setCreateAt(new \DateTime());
        $this->em->persist($job);
    }

    public function getAllJobsAlertUserJson()
    {
        $jobRipo = $this->em->getRepository(JobAlertUser::class);
        $jobusers = $jobRipo->findAll();
        $jobusersJson = $this->serializer->serialize($jobusers, 'json');
        return $jobusersJson;
    }

    public function CreateNewJobsAlertUserJson()
    {
        $jobRipo = $this->em->getRepository(JobAlertUser::class);
        $jobusers = $jobRipo->findAll();
        $jobusersJson = $this->serializer->serialize($jobusers, 'json');
        return $jobusersJson;
    }

    //this code is testing purpose
    public function checkAndConvertJsonData($a){
        $jobusersJson = $this->serializer->serialize($a, 'json');
        return $jobusersJson;
    }

    //persist every new object
    public function persistObjectToDatabase($object)
    {
        $this->em->persist($object);

    }

    //convert any object to json
    public function convertObjectToJson($object)
    {
        return  $this->serializer->serialize($object, 'json');
    }

    public function saveJsonPostData($request,$id = null)
    {

        $jobCateRipo = $this->em->getRepository(JobCategory::class);
        $jobAlertRipo = $this->em->getRepository(JobAlertUser::class);
        $jobRoleRipo = $this->em->getRepository(Role::class);
        $jobLabelRipo = $this->em->getRepository(Label::class);
        $data = json_decode($request);


        $jobAlertUser = new JobAlertUser();

        if($id)
        {
            $jobAlertUser = $jobAlertRipo->find($id);
        }
        if(!isset($data->email)) throw new \Exception('Email field can not be null');
        $email = $data->email;

// this part of code is commented for implementing new logic where user can create multiple alert by a single email
//    $hashValue = hash('sha256',$email);
// check existing job alert exist or not
//
//        $existingAlert = $jobAlertRipo->findOneBy(['email'=>$email]);
//        if($existingAlert)
//        {
//
//            if($existingAlert->getStatus())
//            {
//                throw  new \Exception('Active Alert with this email already exist');
//            }
//
//        }
  // end existing job alert

        $jobAlertUser->setEmail($email);
//        $jobAlertUser->setTokenfield($hashValue.time());
        if(!isset($data->keywords)) throw new \Exception('One or More keyword must be inserted');
        $keyword = $data->keywords;
        $jobAlertUser->setKeywords($keyword);
        $jobAlertUser->setFullName(isset($data->full_name)? $data->full_name: '');
        $jobAlertUser->setNameOfAlertedJob(isset($data->name_of_alerted_job)?$data->name_of_alerted_job:'');
        $jobAlertUser->setLocation(isset($data->location)? $data->location:'');
        $jobAlertUser->setWorkExperience(isset($data->work_experience)?$data->work_experience:null);
        $jobAlertUser->setExpectedSalary(isset($data->expected_salary)?$data->expected_salary:null);
        $jobCategoryId = isset($data->job_category_id)? $data->job_category_id:null;
        $jobAlertUser->setJobCategoryId(isset($data->job_category_id)?$data->job_category_id:null);
        $jobCategory = $jobCateRipo->find($jobCategoryId);
        $jobAlertUser->setJobCategoryName($jobCategory->getName());
        $jobAlertUser->setActivateAt(isset($data->activate_at)?new DateTime($data->activate_at):null);
        $jobAlertUser->setCompanyId(isset($data->company_id)?$data->company_id:null);
        $jobAlertUser->setCompanyName(isset($data->company_name)?$data->company_name:"");
        $jobAlertUser->setIsSentCloseJobFlag(isset($data->is_sent_close_job_flag)?$data->is_sent_close_job_flag:false);

        $role = null;
        $label = null;
        if( isset($data->role_id))
        {
            $role = $jobRoleRipo->find($data->role_id);
        }

        if( isset($data->job_label_id))
        {
            $label = $jobLabelRipo->find($data->job_label_id);
        }

        $jobAlertUser->setRoleId(isset($role)?$role->getId():null);
        $jobAlertUser->setRoleName(isset($role)?$role->getName():null);
        $jobAlertUser->setJobLabelId(isset($label)?$label->getId():null);
        $jobAlertUser->setJobLabelName(isset($label)?$label->getId():null);
        $jobAlertUser->setTermAndConditionSlug(isset($data->termsConditions)?$data->termsConditions:null);
        $jobAlertUser->setCreateAt(new DateTime());

        if(isset($data->activate_at))
        {
            $jobAlertUser->setStatus(1);
        }
        else
        {
            $jobAlertUser->setStatus(2);
        }

        // check if the alert user is job user or not
        // new logic implemented
        if(true||$this->checkAlertUserIsJobUser($jobAlertUser->getEmail()))
        {
            $jobAlertUser->setIsRegister(true);
        }
        else
        {
            $jobAlertUser->setIsRegister(false);
        }

        if(!$id) $this->em->persist($jobAlertUser);
        return $jobAlertUser;
    }

    public function getAllJobCategory()
    {
        $jobCatRipo = $this->em->getRepository(JobCategory::class);
        $jobRoleRipo = $this->em->getRepository(Role::class);
        $jobLableRipo = $this->em->getRepository(Label::class);
        $allActiveRole = $jobRoleRipo->findBy(['isActive'=> 1]);
        $allActiveLabel = $jobLableRipo->findBy(['isActive'=> 1]);
        $allCategory = $jobCatRipo->findAll();

        $response = new stdClass();
        $response->allActiveRole =  $allActiveRole;
        $response->allActiveLabel =  $allActiveLabel;
        $response->allCategory =  $allCategory;

        return $this->convertObjectToJson($response);

    }

    public function createNewJobCategory($request)
    {
        // receive only job category title as json format
        $data = json_decode($request);
        $jobCategory = new JobCategory();
        $jobCategory->setName($data->title);
        $jobCategory->setStatus(true);
        $this->em->persist($jobCategory);
        return $this->convertObjectToJson($jobCategory);

    }

    public function createNewJobUser($email,$alertId)
    {
        $url = null;

        if(!$this->checkAlertUserIsJobUser($email)) {
 // this email is not have any job user so request to create a user first

//            $this->httpClient->request('POST', self::GATEWAY_URL.'/emails/v1/confirm-jobalert',[
//                'headers'=>[
//                    'Authorization'=>'apikey '.self::EMAIL_SERVICE_API_KEY
//                ],
//                'json'=>['email'=>$email,'confirmLink'=>self::FRONTEND_URL.'/users/signup?email='.$email.'&confirm_email=true&redirect_url=/job-alerts/'.$alertId.'/confirm']
//            ]);
            return true;

        }
        else
        {
 // the user  email already exist is job service
 // just activate the alert
            $this->changeAlertStatus($email,2);
            return false;

        }


    }

    public function sendAlertConfirmationMail($email, $isRegister) {
        if(!$isRegister ) {
            $this->httpClient->request('POST', self::GATEWAY_URL.'/emails/v1/confirm-jobalert',[
                'headers'=>[
                    'Authorization'=>'apikey '.self::EMAIL_SERVICE_API_KEY
                ],
                'json'=>['email'=>$email,'signupLink'=>self::FRONTEND_URL.'/users/signup?email='.$email,'redirect_url' => self::GATEWAY_URL.'/job-alerts/email/'.$email.'/activate-register-status']
            ]);
        }
    }

    public function checkAlertUserIsJobUser($email):bool
    {
        $response = $this->httpClient->request('GET', self::GATEWAY_URL.'/users/'.$email.'/exists');
        $statusCode = $response->getStatusCode();
        if($statusCode == 200)
        {
            return true;
        }

        return false;
    }

    public function changeAlertStatus($email, $status)
    {
        $this->em->getRepository(JobAlertUser::class)->findOneBy(['email'=>$email],['createAt'=>'DESC'])->setStatus($status);
    }

    public function activateAlertAfterUserCreation($id)
    {
        $email = $this->em->getRepository(JobAlertUser::class)->find($id)->getEmail();

// check the job user already created or not
        $userIsExist = $this->checkAlertUserIsJobUser($email);

        if($userIsExist)
        {
            $this->changeAlertStatus($email,2);

            return true;
        }
        else
        {
            return false;
        }

    }

    public function getJobAlertUserList($page, $limit)
    {
        $repository = $this->em->getRepository(JobAlertUser::class);
        $pagination = $this->paginator->paginate(
            $repository->findBy(['status'=>2],['createAt'=>'ASC']),
            $page,
            $limit
        );

        return $pagination;
    }

    public function getJobALertJobList($jobCategoryId)
    {
        $jobRipo = $this->em->getRepository(Jobs::class);
        $jobList = $jobRipo->getUnAlertedJobByCategory($jobCategoryId);
        return $jobList;

    }

    public function getJobAlertedPaginatedList($jobCategoryId,$page,$limit)
    {
        $repository = $this->em->getRepository(Jobs::class);
        $pagination = $this->paginator->paginate(
            $repository->findAll(['categoryId'=>$jobCategoryId,'jobStatus'=>1],['createAt'=>'ASC']),
            $page,
            $limit
        );
        return $pagination;
    }

    public function jobDetails($id) {
        $job = $this->em->getRepository(Jobs::class)->find($id);
//        if(!$job) throw new \Exception(' no such job exist');
        return $this->convertObjectToJson($job);
    }

    public function seeMoreJob($email)
    {

        $repository = $this->em->getRepository(JobAlertUser::class);
        $alert = $repository->findOneBy(['email'=>$email]);
        $categoryId = $alert->getJobCategoryId();
        $sendAlertInfo = $alert->getCurrentAlertPageNum();
        $pageNum = $sendAlertInfo['jobPage'] + 1;
        $seeMorePage = $pageNum;

        if(isset($sendAlertInfo['seeMore'])){
            $seeMorePage = $sendAlertInfo['seeMore'];
        }

        $limit = 10;
        $pagination = $this->paginator->paginate(
            $this->em->getRepository(Jobs::class)->findBy(['categoryId'=>$categoryId, 'jobStatus'=>1],['createAt'=>'ASC']),
            $page=$seeMorePage,
            $limit
        );
        if($pagination->getTotalItemCount()> $limit*$seeMorePage)
        {
            $alert->setCurrentAlertPageNum((array('jobPage'=>$sendAlertInfo['jobPage'],'seeMore'=> $seeMorePage+1)));

        }


        return $pagination;

    }

    public function getAllJobsCategoy() {
        $jobCatRipo = $this->em->getRepository(JobCategory::class);
        $allCategory = $jobCatRipo->findAll();
        return $this->convertObjectToJson($allCategory);
    }

    public function sendJobToJobAlert($data,$email)
    {
        $response = $this->httpClient->request('POST', self::GATEWAY_URL.'/emails/v1/send-job-alert',[
            'headers'=>[
                'Authorization'=>'apikey '.self::EMAIL_SERVICE_API_KEY
            ],
            'json' => ['email'=>$email,'data'=> $data]

        ]);
        $statusCode = $response->getStatusCode();
        if($statusCode == 200)
        {
            return true;
        }

        return false;
    }

    public function activateAlertUserRegisterStatus(string $email)
    {
        $jobAlertRipo = $this->em->getRepository(JobAlertUser::class);
        $jobAlerts = $jobAlertRipo->findBy(['email'=>$email], ['createAt'=> 'DESC']);
        foreach ($jobAlerts as $jobAlert)
        {
            $jobAlerts->setIsRegister(true);
        }
    }


    // job section

    public function getJobDetails(string $id)
    {
        $job = $this->em->getRepository(Jobs::class)->find($id);
        if(!$job) throw new \Exception(' no such job exist');
        return $job;
    }

    public function getLoginUserDetails($userId, $consumerId)
    {
        $client = HttpClient::create();

        $gatewayData = $client->request('GET',self::GATEWAY_ADMIN_URL.'/tokens/'.$consumerId,[
            'headers'=>[
                'Authorization'=>'apikey '.self::GATEWAY_ADMIN_API_KEY
            ]
        ]);

        $status = $gatewayData->getStatusCode();
        if($status != 200){
            throw new Exception("can not access this page");
        }

        $tokens = $gatewayData->toArray();
        $userData = $client->request('GET',self::GATEWAY_URL.'/users/'.$userId,[
            'headers'=>[
                'Authorization'=>'Bearer '.$tokens['access_token'],
                'Content-Type'=>'application/json'
            ]
        ]);

        if($userData->getStatusCode() != 200){
            throw new Exception("User not exist");
        }
        return $userData->toArray();
    }

    // call when the login user is user or not
    public function isCompanyType($userId)
    {
        $client = HttpClient::create();
//        $userData = $client->request('GET',self::GATEWAY_URL.'/companies/user/'.$userId,[
// locally testing purpose
        $userData = $client->request('GET',self::GATEWAY_URL.'/companies/user/'.$userId,[
            'headers'=>[
                'Content-Type'=>'application/json'
            ]
        ]);
        if( $userData->getStatusCode() == 200) return true;
        return false;
    }

    public function getCompanyJob(?string $userId,$page=1,$limit=20)
    {
        $repository = $this->em->getRepository(Jobs::class);
        $jobs = $repository->findBy(['userId'=>$userId],['liveAt'=>"DESC",'createAt'=>'DESC']);
        $paginator = $this->paginator->paginate($jobs,$page,$limit);
        $response = new stdClass();
        $response->jobs = $paginator->getItems();
        $response->pageNumber = $paginator->getCurrentPageNumber();
        $response->totalCount = $paginator->getTotalItemCount();
        $response->limit = $paginator->getItemNumberPerPage();
        return $response;
    }

    public function getAllJob(?string $userId)
    {
        $repository = $this->em->getRepository(Jobs::class);
        $jobs = $repository->findBy(['userId'=>$userId],['liveAt'=>"DESC",'createAt'=>'DESC']);
        return $jobs;
    }

    public function getSeekerJobs( int $page = 1, int $limit = 10)
    {

        $repository = $this->em->getRepository(Jobs::class);
        $pagination = $this->paginator->paginate($repository->findAll(), $page, $limit);
        return $pagination;
    }

    public function makeJobLive(string $id, string $subscriptionId='',  $token)
    {
        $job = $this->em->getRepository(Jobs::class)->find($id);
        if(!$job) throw new \Exception(' no such job exist',404);

        // make a request to company service verify the validity of company
        $companyId = $job->getCompanyId();
        $companyRequest = self::makeHttpCall('GET',self::GATEWAY_URL.'/companies/'.$companyId.'/verify',$token['access_token']);
 
        if(false && $companyRequest->getStatusCode() != 200)
        {
            throw new \Exception('Company is not verified',404);
        }

        if($job->getEndAt() != null && ($job->getEndAt() <= new DateTime()) ){
            throw new \Exception(' This job already expired',423);
        }
        $job->setJobStatus(1);
        $job->setIslived(true);
        if($job->getLiveAt() == null) {
            $job->setLiveAt(new DateTime());
            //$job->setEndAt(new DateTime());
            // check if any default subscription exists
            $client = HttpClient::create();
            $subscriptionData = $client->request('GET',self::GATEWAY_URL.'/subscriptions/'.$subscriptionId);
            $subscriptionStatus = $subscriptionData->getStatusCode();

            if($subscriptionStatus == 200){
                $SD = $subscriptionData->toArray();
                $today = new DateTime() ;
                $endDate = new DateTime() ;

                foreach($SD['benefits'] as $benefit){
                    if($benefit['job_post_expire_in_days'] != null){
                        $endDate = $today->add(new DateInterval('P'.$benefit['job_post_expire_in_days'].'D'));
                    }
                }
                $job->setEndAt($endDate);

            }

        }

        $activeJob = $this->transferToAActiveList($id, $subscriptionId);
        return $activeJob;
    }

    public function getDraftJob(?string $userId, string $page = "1", string  $limit = "10")
    {
        $pagination = $this->paginator->paginate($this->em->getRepository(Jobs::class)->getDraftedJob($userId), $page, $limit);
        return $pagination;
    }

    public function getMyJobs($userId, string $page, string $limit)
    {
        $pagination = $this->paginator->paginate($this->em->getRepository(Jobs::class)->findBy(['userId'=>$userId],['createAt'=>'DESC']), $page, $limit);
        return $pagination;
    }
    public function getExpiredJobs($userId, string $page, string $limit)
    {
        $pagination = $this->paginator->paginate($this->em->getRepository(Jobs::class)->findBy(['userId'=>$userId,'isExpired'=>1],['createAt'=>'DESC']), $page, $limit);
        return $pagination;
    }
    public function editJob($jobData, $jobId)
    {

        $response = new \stdClass;
        $response->error = null;

        try{

            $job = $this->em->getRepository(Jobs::class)->find($jobId);
            $jobTypeRepo = $this->em->getRepository(JobType::class);
            $jobData = json_decode($jobData);


            if($job != null) {


                if (isset($jobData->currency_code)) {
                    $job->setCurrencyCode($jobData->currency_code);
                }


                if (isset($jobData->job_title)) {
                    $job->setJobTitle($jobData->job_title);
                } else {
                    throw new \Exception("Job Title is required");
                }


                if (isset($jobData->job_description)) {
                    $job->setJobDescription($jobData->job_description);
                } else {
                    throw new \Exception("Job Description is required");
                }


                if (isset($jobData->type)) {
                    $typeInText = $this->em->getRepository(JobType::class)->findOneBy(['type'=>$jobData->type])->getTypeInText();
                    $job->setTypeInText($typeInText);
                    $job->setType($jobData->type);
                } else {
                    throw new \Exception("Job Type is required  is required like part time fulltime");
                }

                if (isset($jobData->company_id)) {
                    $job->setCompanyId($jobData->company_id);
                } else {
                    throw new \Exception("Company ID is required ");
                }

                if (isset($jobData->company_name)) {
                    $job->setCompanyName($jobData->company_name);
                } else {
                    throw new \Exception("Company Name is required ");
                }

                if (isset($jobData->job_location)) {
                    $job->setJobLocation($jobData->job_location);
                } else {
                    throw new \Exception("Job Location  is required");
                }

                if (isset($jobData->country)) {
                    $job->setCountry($jobData->country);
                } else {
                    throw new \Exception("Country  is required ");
                }

                if (isset($jobData->city)) {
                    $job->setCity($jobData->city);
                } else {
                    throw new \Exception("City  is required");
                }

                if (isset($jobData->apply_instruction)) {
                    $job->setApplyInstruction($jobData->apply_instruction);
                }

                if (isset($jobData->state)) {
                    //not requied field
                    $job->setState($jobData->state);
                }

                if (isset($jobData->zip_code)) {
                    //not requied field
                    $job->setZipCode($jobData->zip_code);
                }

                if (isset($jobData->min_salary_range)) {
                    $job->setMinSalaryRange($jobData->min_salary_range);
                } else {
                    throw new \Exception("Min Salary  is required");
                }

                if (isset($jobData->max_salary_range)) {
                    $job->setMaxSalaryRange($jobData->max_salary_range);
                } else {
                    throw new \Exception("Max Salary  is required");
                }

                $job->setUpdateAt(new \DateTime());
                if (isset($jobData->job_facilities)) {
                    $jobFacilitiesRipo = $this->em->getRepository(JobFacility::class);
					if(is_array($jobData->job_facilities)){
						foreach ($jobData->job_facilities as $job_facility) {
							$jobFacility = new JobFacility();
							if (isset($job_facility->id)) {
								$jobFacility = $jobFacilitiesRipo->find($job_facility->id);
								$jobFacility->setText(isset($job_facility->text)?$job_facility->text:'');
								$jobFacility->setTitle(isset($job_facility->title)?$job_facility->title:'');
							} else {
								$jobFacility->setText(isset($job_facility->text)?$job_facility->text:'');
								$jobFacility->setTitle(isset($job_facility->title)?$job_facility->title:'');
								$this->em->persist($jobFacility);
							}
							$job->removeAllJobFacility();
							$job->addJobFacility($jobFacility);
						}
					}
                } else {
                    throw new \Exception("Job Facilities  is required");
                }

                if (isset($jobData->job_responsibilities)) {
                    $jobResponsibilityRipo = $this->em->getRepository(JobResponsibility::class);
                    $job->removeAllJobResponsibility();
					if(is_array($jobData->job_responsibilities)){
						foreach ($jobData->job_responsibilities as $job_responsibility) {
							$jobResponsibility = new JobResponsibility();
							if (isset($job_responsibility->id)) {
								$jobResponsibility = $jobResponsibilityRipo->find($job_responsibility->id);
								$jobResponsibility->setText(isset($job_responsibility->text)?$job_responsibility->text:'');
								if (isset($job_responsibility->is_additional)) {
									$jobResponsibility->setIsAdditional($job_responsibility->is_additional);
								} else {
									$jobResponsibility->setIsAdditional(false);
								}
							} else {
								$jobResponsibility->setText($job_responsibility->text);

								if (isset($job_responsibility->is_additional)) {
									$jobResponsibility->setIsAdditional($job_responsibility->is_additional);
								} else {
									$jobResponsibility->setIsAdditional(false);
								}

								$this->em->persist($jobResponsibility);
							}
							if(isset($job_responsibility->is_deleted) && $job_responsibility->is_deleted)
							{
								$job->removeJobResponsibility($jobResponsibility);
							}
							else
							{
								$job->addJobResponsibility($jobResponsibility);
							}


						}
					}
                } else {
                    throw new \Exception("Job Responsibility  is required");
                }

                if (isset($jobData->skills)) {
                    $jobSkillsRipo = $this->em->getRepository(Skill::class);
                    $job->removeAllSkill();
					if(is_array($jobData->skills)){
						foreach ($jobData->skills as $job_skill) {
							$skill = new Skill();
							if (isset($job_skill->id)) {
								$skill = $jobSkillsRipo->find($job_skill->id);
							} else {
								$skill->setTitle($job_skill->title);
								$this->em->persist($skill);
							}

							if(isset($job_skill->is_deleted) && $job_skill->is_deleted)
							{
								$job->removeSkill($skill);
							}
							else
							{
								$job->addSkill($skill);
							}

						}
					}
                } else {
                    throw new \Exception("Job Skills  is required");
                }
                if (isset($jobData->job_education_req)) {
                    $jobEducationalReqRipo = $this->em->getRepository(JobEducationReq::class);
                    $job->removeAllJobEducationReq();
					if(is_array($jobData->job_education_req)){
						foreach ($jobData->job_education_req as $educationalReq) {
							$jobEducationalReq = new JobEducationReq();
							if (isset($educationalReq->id)) {
								$jobEducationalReq = $jobEducationalReqRipo->find($educationalReq->id);
								$jobEducationalReq->setDegreTitle($educationalReq->degre_title);

							} else {
								$jobEducationalReq->setDegreTitle($educationalReq->degre_title);
								$this->em->persist($jobEducationalReq);
							}

							if(isset($educationalReq->is_deleted) && $educationalReq->is_deleted)
							{
								$job->removeJobEducationReq($jobEducationalReq);
							}
							else
							{
								$job->addJobEducationReq($jobEducationalReq);
							}
						}
					}
                } else {
                    throw new \Exception("Educational Requirement  is required");
                }

                if (isset($jobData->job_category)) {
                    $categoryRipo = $this->em->getRepository(JobCategory::class);
                    $job->removeAllJobCategory();
					if(is_array($jobData->job_category)){
						foreach ($jobData->job_category as $categoryData) {
							$category = new JobCategory();
							if (isset($categoryData->id)) {
								$category = $categoryRipo->find($categoryData->id);
							} else {
								$category->setName($categoryData->name);
								$category->setStatus(true);
								$this->em->persist($category);

							}
							if(isset($categoryData->is_deleted) && $categoryData->is_deleted)
							{
								$job->removeJobCategory($category);
							}
							else
							{
								$job->addJobCategory($category);
							}
						}
					}
                } else {
                    throw new \Exception("Job Category is required");
                }

                $response->job  = $job;
                return $response;
            }

        } catch (\Exception $e)
        {
            $response->job  = null;
            $response->error = $e->getMessage();
            return $response;
        }
    }

    public function transferToAActiveList($jobid, $subscriptionId)
    {

        $job = $this->em->getRepository(Jobs::class )->find($jobid);
        $job->setSubscriptionId($subscriptionId);

        $activeJob = new ActiveJobs();
        $activeJob->setCompanyLogo($job->getCompanyLogo());
        $activeJob->setJobTitle($job->getJobTitle());
        $activeJob->setJobDescription( $job->getJobDescription());
        $activeJob->setJobStatus(1);
        $activeJob->setType( $job->getType());
        $activeJob->setCreateAt( $job->getCreateAt());
        $activeJob->setTypeInText( $job->getTypeInText());
        $activeJob->setLiveAt($job->getLiveAt());
        $activeJob->setEndAt($job->getEndAt());
        $activeJob->setIsLived(true);
        $activeJob->setUpdateAt( $job->getUpdateAt());
        $activeJob->setCompanyId( $job->getCompanyId());
        $activeJob->setCompanyName( $job->getCompanyName());
        $activeJob->setJobLocation( $job->getJobLocation());
        $activeJob->setZipCode( $job->getZipCode());
        $activeJob->setCity( $job->getCity());
        $activeJob->setCountry( $job->getCountry());
        $activeJob->setState( $job->getState());
        $activeJob->setSource( $job->getSource());
        $activeJob->setMinSalaryRange( $job->getMinSalaryRange());
        $activeJob->setMaxSalaryRange( $job->getMaxSalaryRange());
        $activeJob->setPriority( $job->getPriority());
        $activeJob->setApplyToEmail( $job->getApplyToEmail());
        $activeJob->setApplyInstruction( $job->getApplyInstruction());
        $activeJob->setGeneralApply( $job->getGeneralApply());
        $activeJob->setApplied( $job->getApplied());
        $activeJob->setIsAlerted( false);
        $activeJob->setRate($job->getRate()?$job->getRate():0.0);
        $activeJob->setPartialViewCount( $job->getPartialViewCount());
        $activeJob->setDetailViewCount( $job->getDetailViewCount());
        $activeJob->setClickViewCount( $job->getClickViewCount());
        $activeJob->setUserId( $job->getUserId());
        $activeJob->setJobId( $job->getId());
        $activeJob->setSubscriptionId($subscriptionId);
        $activeJob->setCurrencyCode($job->getCurrencyCode());

        $jobCategory = $job->getJobCategory();

        $activeJobCategoryRipo = $this->em->getRepository(ActiveJobCategory::class);
        foreach ($jobCategory as $category) {
                $categoryId = $category->getId();
                $activeJobCategory = $activeJobCategoryRipo->findOneBy(["jobCategoryId"=>$categoryId]);
                if(!$activeJobCategory) {
                    $activeJobCategory = new ActiveJobCategory();
                    $activeJobCategory->setName($category->getName());
                    $activeJobCategory->setStatus(1);
                    $activeJobCategory->setJobCategoryId($category->getId());
                    $this->em->persist($activeJobCategory);
                }
            $activeJob->addJobCategory($activeJobCategory);
        }


        $jobEducationReq = $job->getJobEducationReq();
        $activeJobEducationReq = $this->em->getRepository(ActiveJobEducationReq::class);
        foreach ($jobEducationReq as $educationAlReq) {
            $educationAlReqId = $educationAlReq->getId();
            $activeEducationAlReq = $activeJobEducationReq->findOneBy(["jobEducationReqId"=>$educationAlReqId]);
            if(!$activeEducationAlReq) {
                $activeEducationAlReq = new ActiveJobEducationReq();
                $activeEducationAlReq->SetDegreTitle($educationAlReq->getDegreTitle());
                $activeEducationAlReq->setJobEducationReqId($educationAlReqId);
                $this->em->persist($activeEducationAlReq);
            }
            $activeJob->addJobEducationReq($activeEducationAlReq);
        }

        $jobFacilities = $job->getJobFacilities();
             foreach ($jobFacilities as $jobFacility) {
           $activeJobFacility = new ActiveJobFacility();
           $activeJobFacility->setTitle($jobFacility->getTitle());
           $activeJobFacility->setText($jobFacility->getText());
           $activeJobFacility->setJobFacilityId($jobFacility->getId());
           $this->em->persist($activeJobFacility);
           $activeJob->addJobFacility($activeJobFacility);
        }

         $jobResponsibilities = $job->getJobResponsibilities();
        foreach ($jobResponsibilities as $jobResponsibility) {
            $activeJobResponsibility = new ActiveJobResponsibility();
            $activeJobResponsibility->setText($jobResponsibility->getText());
            $activeJobResponsibility->setIsAdditional($jobResponsibility->getIsAdditional());
            $activeJobResponsibility->setJobResponsibilityId($jobResponsibility->getId());
            $this->em->persist($activeJobResponsibility);
            $activeJob->addJobResponsibility($activeJobResponsibility);
        }

        $skilles = $job->getSkills();

        $skillRipo = $this->em->getRepository(ActiveSkill::class);
        foreach ($skilles as $skill) {
            $skillId = $skill->getId();
            $activeSkill = $skillRipo->findOneBy(['skillId'=>$skillId]);
            if(!$activeSkill) {
                $activeSkill = new ActiveSkill();
                $activeSkill->setTitle($skill->getTitle());
                $activeSkill->setSkillId($skill->getId());

                $activeSkill->setCreatedAt($skill->getCreatedAt());
                $activeSkill->setUpdateAt($skill->getUpdateAt());

                $this->em->persist($activeSkill);

            }
            $activeJob->addSkill($activeSkill);


        }
        $this->em->persist($activeJob);
        return $activeJob;
    }

    public function removeExpiredJob()
    {
        $activeJobRipo = $this->em->getRepository(ActiveJobs::class);
        $jobRipo = $this->em->getRepository(Jobs::class);
        $expiredJobs = $activeJobRipo->findByDateExpired();
        $activeJobIds = [];
        $jobIds = [];
        foreach ($expiredJobs as $expiredJob){
           $this->em->remove($activeJobRipo->find($expiredJob['id']));
           $job = $jobRipo->find($expiredJob['jobId']);
           $job->setJobStatus(2);
           $job->setUpdateAt(new DateTime());
        }

        return $expiredJobs;
    }

    public function getSubscriptionInfo($subscriptionId=1, $consumerId=1)
    {
        // Rajon vai service is dependent

        $accessToken = $this->getAccessToken($consumerId);
        $subscriptionServiceCall = $this->httpClient->request('GET', self::GATEWAY_URL.'/subscription/'.$subscriptionId,[
                'headers'=>[
                    'Authorization'=>'Bearer '.$accessToken
                ]
            ]);

        $status = $subscriptionServiceCall->getStatusCode();

        if($status != 200){
            return null;
        }
         return $subscriptionServiceCall->toArray();
    }

    public function updateSubscription()
    {

    }

    public function  getAccessToken($consumerId)
    {
        $gatewayData = $this->httpClient->request('GET',self::GATEWAY_URL.'/tokens/'.$consumerId,[
            'headers'=>[
                'Authorization'=>'apikey '.self::GATEWAY_ADMIN_API_KEY
            ]
        ]);

        $status = $gatewayData->getStatusCode();
        if($status != 200){
            return "";
        }
        $tokens = $gatewayData->toArray();
        return $tokens['access_token'];

    }

    public function getCandidateExperienceLable()
    {
       $candidateExperienceLabel = [];
       $candidateExperienceLabel[] = new ExpertizeLabel('fresher Level','Fresher Level');
       $candidateExperienceLabel[] = new ExpertizeLabel('mid Level','Mid Level');
       $candidateExperienceLabel[] = new ExpertizeLabel('expert Level','Expert Level');
       return $candidateExperienceLabel;
    }

    public function getApplicantIdList(?string $userId, string $jobId, int $limit = 20, int $page = 1)
    {
        $applicantList = Array();
        $appllierRepo = $this->em->getRepository(JobAppliers::class);
        $applicantList = $appllierRepo->findBy(['jobId'=>$jobId]);
        $paginator = $this->paginator->paginate($applicantList, $page, $limit );

        foreach ($paginator->getItems() as $item)
        {
            array_push($applicantList, $item->getUserId());
        }
        return $applicantList;
    }

    public function getAllAppliedJob(string $userId,$page,$limit,$fromDate,$toDate,$companyName)
    {
        $jobApplierRepo = $this->em->getRepository(JobAppliers::class);
        $applierJob = $jobApplierRepo->getAllAppliedJob($userId,$page,$limit,$fromDate,$toDate,$companyName);
        return $applierJob;
    }

    public function disabledActiveJob(string $userId, string $jobId) {
        $activeJobRepo = $this->em->getRepository(ActiveJobs::class);
        $jobRepo = $this->em->getRepository(Jobs::class);

        // remove job from active job table
        // and change the database status

        $activeJob = $activeJobRepo->findOneBy(['jobId'=>$jobId]);
        $this->em->remove($activeJob);

        // update the status in job table
        $job = $jobRepo->find($jobId);
        $job->setJobStatus(0);
        $job->setIsLived(null);
        $job->setUpdateAt(new \DateTime());
    }

    public function isJobCreateByThisUser(string $userId, string $jobId)
    {
        $jobRepo = $this->em->getRepository(Jobs::class );
        $job = $jobRepo->findOneBy(['userId'=>$userId,'id'=>$jobId]);
        if($job) return true;
        return false;
    }

    public function updateResumeServiceJobApplier($jobId,$applierId, $token)
    {

        $path = self::GATEWAY_URL.'/resume/job/'.$jobId.'/userid/'.$applierId.'/shortlist';
        $updateResumeTable = self::makeHttpCall('PUT',$path ,$token);
        if($updateResumeTable->getStatusCode() == 200) {
            return true;
        }
        return false;

    }


}