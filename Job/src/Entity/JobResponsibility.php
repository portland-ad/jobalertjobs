<?php

namespace App\Entity;

use App\Repository\JobResponsibilityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=JobResponsibilityRepository::class)
 */
class JobResponsibility
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $text;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAdditional;

    /**
     * @ORM\ManyToOne(targetEntity=Jobs::class, inversedBy="jobResponsibilities")
     */
    private $jobs;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getIsAdditional(): ?bool
    {
        return $this->isAdditional;
    }

    public function setIsAdditional(?bool $isAdditional): self
    {
        $this->isAdditional = $isAdditional;

        return $this;
    }

    public function getJobs(): ?Jobs
    {
        return $this->jobs;
    }

    public function setJobs(?Jobs $jobs): self
    {
        $this->jobs = $jobs;

        return $this;
    }
}
