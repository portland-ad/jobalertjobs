<?php

namespace App\Entity;

use App\Repository\JobEducationReqRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ActiveJobEducationReqRepository::class)
 */
class ActiveJobEducationReq
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $degreTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobEducationReqId;

//    /**
//     * @ORM\ManyToMany(targetEntity=ActiveJobs::class, mappedBy="jobEducationReq")
//     */
//    private $activeJobs;




    public function __construct()
    {
//        $this->jobs = new ArrayCollection();
        $this->activeJobs = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDegreTitle(): ?string
    {
        return $this->degreTitle;
    }

    public function setDegreTitle(string $degreTitle): self
    {
        $this->degreTitle = $degreTitle;

        return $this;
    }

//    /**
//     * @return Collection|ActiveJobs[]
//     */
//    public function getActiveJobs(): Collection
//    {
//        return $this->activeJobs;
//    }
//
//    public function addActiveJob(ActiveJobs $activeJob): self
//    {
//        if (!$this->activeJobs->contains($activeJob)) {
//            $this->activeJobs[] = $activeJob;
//            $activeJob->addJobEducationReq($this);
//        }
//
//        return $this;
//    }
//
//    public function removeActiveJob(ActiveJobs $activeJob): self
//    {
//        if ($this->activeJobs->contains($activeJob)) {
//            $this->activeJobs->removeElement($activeJob);
//            $activeJob->removeJobEducationReq($this);
//        }
//
//        return $this;
//    }


public function getJobEducationReqId(): ?string
{
    return $this->jobEducationReqId;
}

public function setJobEducationReqId(string $jobEducationReqId): self
{
    $this->jobEducationReqId = $jobEducationReqId;

    return $this;
}
}
