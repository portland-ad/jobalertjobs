<?php

namespace App\Entity;

use App\Repository\JobEducationReqRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=JobEducationReqRepository::class)
 */
class JobEducationReq
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $degreTitle;

//    /**
//     * @ORM\ManyToMany(targetEntity=Jobs::class, mappedBy="jobEducationReq")
//     */
//    private $jobs;


    public function __construct()
    {
        $this->jobs = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDegreTitle(): ?string
    {
        return $this->degreTitle;
    }

    public function setDegreTitle(string $degreTitle): self
    {
        $this->degreTitle = $degreTitle;

        return $this;
    }
//
//    /**
//     * @return Collection|Jobs[]
//     */
//    public function getJobs(): Collection
//    {
//        return $this->jobs;
//    }
//
//    public function addJob(Jobs $job): self
//    {
//        if (!$this->jobs->contains($job)) {
//            $this->jobs[] = $job;
//            $job->addJobEducationReq($this);
//        }
//
//        return $this;
//    }
//
//    public function removeJob(Jobs $job): self
//    {
//        if ($this->jobs->contains($job)) {
//            $this->jobs->removeElement($job);
//            $job->removeJobEducationReq($this);
//        }
//
//        return $this;
//    }

}
