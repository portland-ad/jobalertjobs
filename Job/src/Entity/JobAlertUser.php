<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JobAlertUserRepository")
 */
class JobAlertUser
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $keywords;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $fullName;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $nameOfAlertedJob;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $location;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $workExperience;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $expectedSalary;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $industry;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\JobCategory")
     */
    private $jobCategory;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $role;


    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $jobCategoryId;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $jobCategoryName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $roleId;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $roleName;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $jobLabelId;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $jobLabelName;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $termAndConditionSlug;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"0 = pending Job Alert, 1 = Inactive but will be activate after a certain period, 2 Active job alert, "})
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $tokenfield;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $activationExpireTime;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $currentAlertPageNumber;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $currentAlertPageNum = [];

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRegister = false;

    /**
     * this field is added for activate the alert after a certain time period
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $activateAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isSentCloseJobFlag = false;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $companyName="";


    public function __construct()
    {
        $this->jobCategory = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(?string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNameOfAlertedJob(): ?string
    {
        return $this->nameOfAlertedJob;
    }

    public function setNameOfAlertedJob(?string $nameOfAlertedJob): self
    {
        $this->nameOfAlertedJob = $nameOfAlertedJob;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getWorkExperience(): ?string
    {
        return $this->workExperience;
    }

    public function setWorkExperience(?string $workExperience): self
    {
        $this->workExperience = $workExperience;

        return $this;
    }

    public function getExpectedSalary(): ?int
    {
        return $this->expectedSalary;
    }

    public function setExpectedSalary(?int $expectedSalary): self
    {
        $this->expectedSalary = $expectedSalary;

        return $this;
    }

    public function getLabel(): ?int
    {
        return $this->label;
    }

    public function setLabel(?int $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getIndustry(): ?string
    {
        return $this->industry;
    }

    public function setIndustry(?string $industry): self
    {
        $this->industry = $industry;

        return $this;
    }

    /**
     * @return Collection|JobCategory[]
     */
    public function getJobCategory(): Collection
    {
        return $this->jobCategory;
    }

    public function addJobCategory(JobCategory $jobCategory): self
    {
        if (!$this->jobCategory->contains($jobCategory)) {
            $this->jobCategory[] = $jobCategory;
        }

        return $this;
    }

    public function removeJobCategory(JobCategory $jobCategory): self
    {
        if ($this->jobCategory->contains($jobCategory)) {
            $this->jobCategory->removeElement($jobCategory);
        }

        return $this;
    }

    public function getRole(): ?int
    {
        return $this->role;
    }

    public function setRole(?int $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getJobCategoryId(): ?string
    {
        return $this->jobCategoryId;
    }

    public function setJobCategoryId(?string $jobCategoryId): self
    {
        $this->jobCategoryId = $jobCategoryId;

        return $this;
    }

    public function getJobCategoryName(): ?string
    {
        return $this->jobCategoryName;
    }

    public function setJobCategoryName(?string $jobCategoryName): self
    {
        $this->jobCategoryName = $jobCategoryName;

        return $this;
    }

    public function getRoleId(): ?string
    {
        return $this->roleId;
    }

    public function setRoleId(?string $roleId): self
    {
        $this->roleId = $roleId;

        return $this;
    }

    public function getRoleName(): ?string
    {
        return $this->roleName;
    }

    public function setRoleName(?string $roleName): self
    {
        $this->roleName = $roleName;

        return $this;
    }

    public function getJobLabelId(): ?string
    {
        return $this->jobLabelId;
    }

    public function setJobLabelId(?string $jobLabelId): self
    {
        $this->jobLabelId = $jobLabelId;

        return $this;
    }

    public function getJobLabelName(): ?string
    {
        return $this->jobLabelName;
    }

    public function setJobLabelName(?string $jobLabelName): self
    {
        $this->jobLabelName = $jobLabelName;

        return $this;
    }

    public function getTermAndConditionSlug(): ?bool
    {
        return $this->termAndConditionSlug;
    }

    public function setTermAndConditionSlug(?bool $termAndConditionSlug): self
    {
        $this->termAndConditionSlug = $termAndConditionSlug;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTokenfield(): ?string
    {
        return $this->tokenfield;
    }

    public function setTokenfield(string $tokenfield): self
    {
        $this->tokenfield = $tokenfield;

        return $this;
    }

    public function getActivationExpireTime(): ?string
    {
        return $this->activationExpireTime;
    }

    public function setActivationExpireTime(?string $activationExpireTime): self
    {
        $this->activationExpireTime = $activationExpireTime;

        return $this;
    }

    public function getCurrentAlertPageNumber(): ?string
    {
        return $this->currentAlertPageNumber;
    }

    public function setCurrentAlertPageNumber(?string $currentAlertPageNumber): self
    {
        $this->currentAlertPageNumber = $currentAlertPageNumber;

        return $this;
    }

    public function getCurrentAlertPageNum(): ?array
    {
        return $this->currentAlertPageNum;
    }

    public function setCurrentAlertPageNum(?array $currentAlertPageNum): self
    {
        $this->currentAlertPageNum = $currentAlertPageNum;

        return $this;
    }

    public function getIsRegister(): ?bool
    {
        return $this->isRegister;
    }

    public function setIsRegister(?bool $isRegister): self
    {
        $this->isRegister = $isRegister;

        return $this;
    }

    public function getActivateAt(): ?\DateTimeInterface
    {
        return $this->activateAt;
    }

    public function setActivateAt(?\DateTimeInterface $activateAt): self
    {
        $this->activateAt = $activateAt;

        return $this;
    }

    public function getCompanyId(): ?string
    {
        return $this->companyId;
    }

    public function setCompanyId(?string $companyId): self
    {
        $this->companyId = $companyId;

        return $this;
    }

    public function getIsSentCloseJobFlag(): ?bool
    {
        return $this->isSentCloseJobFlag;
    }

    public function setIsSentCloseJobFlag(?bool $isSentCloseJobFlag): self
    {
        $this->isSentCloseJobFlag = $isSentCloseJobFlag;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }


}
