<?php

namespace App\Entity;

use App\Repository\JobAlertUserEmailTrackerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=JobAlertUserEmailTrackerRepository::class)
 */
class JobAlertUserEmailTracker
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $startJobAlertPage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maxLimit;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getStartJobAlertPage(): ?int
    {
        return $this->startJobAlertPage;
    }

    public function setStartJobAlertPage(?int $startJobAlertPage): self
    {
        $this->startJobAlertPage = $startJobAlertPage;

        return $this;
    }

    public function getMaxLimit(): ?int
    {
        return $this->maxLimit;
    }

    public function setMaxLimit(?int $maxLimit): self
    {
        $this->maxLimit = $maxLimit;

        return $this;
    }
}
