<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActiveJobCategoryRepository")
 */
class ActiveJobCategory
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(name ="title", type="string", length=75, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobCategoryId;

//    /**
//     * @ORM\ManyToMany(targetEntity=ActiveJobs::class, mappedBy="jobCategory")
//     */
//    private $activeJobs;


    public function __construct()
    {
//        $this->jobs = new ArrayCollection();
        $this->activeJobs = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function __toString() {
        return $this->name;
    }

//    /**
//     * @return Collection|ActiveJobs[]
//     */
//    public function getActiveJobs(): Collection
//    {
//        return $this->activeJobs;
//    }
//
//    public function addActiveJob(ActiveJobs $activeJob): self
//    {
//        if (!$this->activeJobs->contains($activeJob)) {
//            $this->activeJobs[] = $activeJob;
//            $activeJob->addJobCategory($this);
//        }
//
//        return $this;
//    }
//
//    public function removeActiveJob(ActiveJobs $activeJob): self
//    {
//        if ($this->activeJobs->contains($activeJob)) {
//            $this->activeJobs->removeElement($activeJob);
//            $activeJob->removeJobCategory($this);
//        }
//
//        return $this;
//    }


public function getJobCategoryId(): ?string
{
    return $this->jobCategoryId;
}

public function setJobCategoryId(string $jobCategoryId): self
{
    $this->jobCategoryId = $jobCategoryId;

    return $this;
}}
