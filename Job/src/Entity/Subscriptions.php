<?php

namespace App\Entity;

use App\Repository\SubscriptionsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SubscriptionsRepository::class)
 */
class Subscriptions
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userId;

    /**
     * @ORM\Column(type="integer")
     */
    private $liveJobCount;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxJobCount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getLiveJobCount(): ?int
    {
        return $this->liveJobCount;
    }

    public function setLiveJobCount(int $liveJobCount): self
    {
        $this->liveJobCount = $liveJobCount;

        return $this;
    }

    public function getMaxJobCount(): ?int
    {
        return $this->maxJobCount;
    }

    public function setMaxJobCount(int $maxJobCount): self
    {
        $this->maxJobCount = $maxJobCount;

        return $this;
    }
}
