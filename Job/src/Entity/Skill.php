<?php

namespace App\Entity;

use App\Repository\SkillRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SkillRepository::class)
 */
class Skill
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)

     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status;

//    /**
//     * @ORM\ManyToMany(targetEntity=Jobs::class, mappedBy="skills")
//     */
//    private $jobs;

    public function __construct()
    {
        $this->jobs = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }
    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

//    /**
//     * @return Collection|Jobs[]
//     */
//    public function getJobs(): Collection
//    {
//        return $this->jobs;
//    }
//
//    public function addJob(Jobs $job): self
//    {
//        if (!$this->jobs->contains($job)) {
//            $this->jobs[] = $job;
//            $job->addSkill($this);
//        }
//
//        return $this;
//    }
//
//    public function removeJob(Jobs $job): self
//    {
//        if ($this->jobs->contains($job)) {
//            $this->jobs->removeElement($job);
//            $job->removeSkill($this);
//        }
//
//        return $this;
//    }


public function getCreatedAt(): ?\DateTimeInterface
{
    return $this->createdAt;
}

public function setCreatedAt(?\DateTimeInterface $createdAt): self
{
    $this->createdAt = $createdAt;

    return $this;
}

public function getUpdateAt(): ?\DateTimeInterface
{
    return $this->updateAt;
}

public function setUpdateAt(?\DateTimeInterface $updateAt): self
{
    $this->updateAt = $updateAt;

    return $this;
}

public function getStatus(): ?int
{
    return $this->status;
}

public function setStatus(?int $status): self
{
    $this->status = $status;

    return $this;
}}
