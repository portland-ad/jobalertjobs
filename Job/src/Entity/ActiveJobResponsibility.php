<?php

namespace App\Entity;

use App\Repository\JobResponsibilityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ActiveJobResponsibilityRepository::class)
 */
class ActiveJobResponsibility
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(name ="title",type="string", length=300)
     */
    private $text;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAdditional;

    /**
     * @ORM\ManyToOne(targetEntity=ActiveJobs::class, inversedBy="ActivejobResponsibilities")
     */
    private $activeJobs;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobResponsibilityId;



    public function getId(): ?string
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getIsAdditional(): ?bool
    {
        return $this->isAdditional;
    }

    public function setIsAdditional(?bool $isAdditional): self
    {
        $this->isAdditional = $isAdditional;

        return $this;
    }

    public function getActiveJobs(): ?ActiveJobs
    {
        return $this->activeJobs;
    }

    public function setActiveJobs(?ActiveJobs $activeJobs): self
    {
        $this->activeJobs = $activeJobs;

        return $this;
    }

    public function getJobResponsibilityId(): ?string
    {
        return $this->jobResponsibilityId;
    }

    public function setJobResponsibilityId(string $jobResponsibilityId): self
    {
        $this->jobResponsibilityId = $jobResponsibilityId;

        return $this;
    }

}
