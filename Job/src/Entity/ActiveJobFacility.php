<?php

namespace App\Entity;

use App\Repository\JobFacilityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ActiveJobFacilityRepository::class)
 */
class ActiveJobFacility
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(name="description",type="text", nullable=true)
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity=ActiveJobs::class, inversedBy="jobFacilities",  cascade={"persist"})
     */
    private $activeJobs;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobFacilityId;



    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getActiveJobs(): ?ActiveJobs
    {
        return $this->activeJobs;
    }

    public function setActiveJobs(?ActiveJobs $activeJobs): self
    {
        $this->activeJobs = $activeJobs;

        return $this;
    }

    public function getJobFacilityId(): ?string
    {
        return $this->jobFacilityId;
    }

    public function setJobFacilityId(string $jobFacilityId): self
    {
        $this->jobFacilityId = $jobFacilityId;

        return $this;
    }
}
