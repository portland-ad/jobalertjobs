<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActiveJobsRepository")
 */
class ActiveJobs
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, options={"comment":"Job Post Title"})
     */
    private $jobTitle;

    /**
     * @ORM\Column(type="text", options={"comment":"Job Description"})
     */
    private $jobDescription;

    /**
     * @ORM\Column(type="integer",options={"comment":"0 = draft job, 1 = active job, 2 = inactive job,  5 = reactiveable job"})
     */
    private $jobStatus;

    /**
     * @ORM\Column(type="integer", options={"comment":"1 = parttime job, 2 = full time job, 3 = time flexible, 4 = remote"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeInText;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"comment":"set specfic date time to live previously created job"}, nullable=true)
     */
    private $liveAt;

    /**
     * this filed is set true when the job is active job
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isLived = false;

    /**
     * @ORM\Column(type="datetime", options={"comment":"set specfic date time to deactivated active job"}, nullable=true)
     */
    private $endAt;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Job Post Date"})
     */
    private $createAt;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"comment":"Updated Date of created job"}, nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="string", length=100,  options={"comment":"Company Id"},)
     */
    private $companyId;

    /**
     * @ORM\Column(type="string", length=100, options={"comment":"Company Name will save here"})
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true,)
     */
    private $jobLocation;

    /**
     * @ORM\Column(type="string", nullable=true, options={"comment":"1 for freasher and entry label, 2 for mid lable, 3 top and above"})
     */
    private $experienceId;

    /**
     * @ORM\Column(type="string", nullable=true,  options={"comment":"level in text"} )
     */
    private $experienceLevel;

    /**
     * @ORM\Column(type="integer", nullable=true,options={"comment":""})
     */
    private $salaryRange;


    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $zipCode;

    /**
     * zip code of location
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $city;

    /**
     * city name
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $country;
    /**
     * country name
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $state;

    /**
     * state of location
     * @ORM\Column(type="string", length=200, nullable=true,options={"Job Comes from out side or company own job"})
     */
    private $source;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $minSalaryRange;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $maxSalaryRange;



    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priority;



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $applyToEmail;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $applyInstruction;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $generalApply;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $applied;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAlerted;

    /**
     * is the job being alerted of not
     * @ORM\Column(type="float", nullable=true)
     */
    private $rate;

    /**
     * numbers of times partial view open
     * @ORM\Column(type="integer", nullable=true)
     */
    private $partialViewCount;

    /**
     * number of times job details view is open
     * @ORM\Column(type="integer", nullable=true)
     */
    private $detailViewCount;

    /**
     * number of times the click in the job
     * @ORM\Column(type="integer", nullable=true)
     */
    private $clickViewCount;

    /**
     * user who post the job
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $userId;

    /**
     * @ORM\ManyToMany(targetEntity=ActiveJobCategory::class, inversedBy="activeJobs")
     */
    private $jobCategory;

    /**
     * @ORM\ManyToMany(targetEntity=ActiveJobEducationReq::class, inversedBy="activeJobs")
     */
    private $jobEducationReq;

    /**
     * @ORM\OneToMany(targetEntity=ActiveJobFacility::class, mappedBy="activeJobs", cascade={"remove"})
     */
    private $jobFacilities;

    /**
     * @ORM\OneToMany(targetEntity=ActiveJobResponsibility::class, mappedBy="activeJobs",cascade={"remove"})
     */
    private $jobResponsibilities;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobId;

    /**
     * @ORM\ManyToMany(targetEntity=ActiveSkill::class)
     */
    private $skills;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subscriptionId;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $currencyCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyLogo;


//    /**
//     * @ORM\Column(type="string", length=255)
//     */
//    private $jobId;

    public function __construct()
    {
        $this->jobCategory = new ArrayCollection();
        $this->jobEducationReq = new ArrayCollection();
        $this->jobFacilities = new ArrayCollection();
        $this->jobResponsibilities = new ArrayCollection();
        $this->skills = new ArrayCollection();
    }






    public function getId(): ?string
    {
        return $this->id;
    }

    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    public function setJobTitle(?string $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    public function getJobDescription(): ?string
    {
        return $this->jobDescription;
    }

    public function setJobDescription(string $jobDescription): self
    {
        $this->jobDescription = $jobDescription;

        return $this;
    }

    public function getJobResponsiblities(): ?string
    {
        return $this->jobResponsiblities;
    }

    public function setJobResponsiblities(?string $jobResponsiblities): self
    {
        $this->jobResponsiblities = $jobResponsiblities;

        return $this;
    }

    public function getJobStatus(): ?int
    {
        return $this->jobStatus;
    }

    public function setJobStatus(?int $jobStatus): self
    {
        $this->jobStatus = $jobStatus;

        return $this;
    }

    public function getLiveAt(): ?\DateTimeInterface
    {
        return $this->liveAt;
    }

    public function setLiveAt(\DateTimeInterface $liveAt): self
    {
        $this->liveAt = $liveAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getCompanyId(): ?string
    {
        return $this->companyId;
    }

    public function setCompanyId(?string $companyId): self
    {
        $this->companyId = $companyId;

        return $this;
    }


    public function getJobLocation(): ?string
    {
        return $this->jobLocation;
    }

    public function setJobLocation(?string $jobLocation): self
    {
        $this->jobLocation = $jobLocation;

        return $this;
    }


    public function getExperienceLavel(): ?string

    {
        return $this->experienceLevel;
    }


    public function setExperienceLevel(?string $experienceLevel): self
    {
        $this->experienceLevel = $experienceLevel;

        return $this;
    }

    public function getSalaryRange(): ?int
    {
        return $this->salaryRange;
    }

    public function setSalaryRange(?int $salaryRange): self
    {
        $this->salaryRange = $salaryRange;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getMinSalaryRange(): ?float
    {
        return $this->minSalaryRange;
    }

    public function setMinSalaryRange(?float $minSalaryRange): self
    {
        $this->minSalaryRange = $minSalaryRange;

        return $this;
    }

    public function getMaxSalaryRange(): ?float
    {
        return $this->maxSalaryRange;
    }

    public function setMaxSalaryRange(?float $maxSalaryRange): self
    {
        $this->maxSalaryRange = $maxSalaryRange;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getExperienceId(): ?string
    {
        return $this->experienceId;
    }

    public function setExperienceId(?string $experienceId): self
    {
        $this->experienceId = $experienceId;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(?int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getTypeInText(): ?string
    {
        return $this->typeInText;
    }

    public function setTypeInText(string $typeInText): self
    {
        $this->typeInText = $typeInText;

        return $this;
    }

    public function getApplyToEmail(): ?string
    {
        return $this->applyToEmail;
    }

    public function setApplyToEmail(?string $applyToEmail): self
    {
        $this->applyToEmail = $applyToEmail;

        return $this;
    }

    public function getApplyInstruction(): ?string
    {
        return $this->applyInstruction;
    }

    public function setApplyInstruction(?string $applyInstruction): self
    {
        $this->applyInstruction = $applyInstruction;

        return $this;
    }

    public function getGeneralApply(): ?int
    {
        return $this->generalApply;
    }

    public function setGeneralApply(?int $generalApply): self
    {
        $this->generalApply = $generalApply;

        return $this;
    }

    public function getApplied(): ?int
    {
        return $this->applied;
    }

    public function setApplied(?int $applied): self
    {
        $this->applied = $applied;
        return $this;
    }
    public function getIsAlerted(): ?bool
    {
        return $this->isAlerted;
    }

    public function setIsAlerted(?bool $isAlerted): self
    {
        $this->isAlerted = $isAlerted;
        return $this;
    }

    public function getRate(): ?float
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getPartialViewCount(): ?int
    {
        return $this->partialViewCount;
    }

    public function setPartialViewCount(?int $partialViewCount): self
    {
        $this->partialViewCount = $partialViewCount;

        return $this;
    }

    public function getDetailViewCount(): ?int
    {
        return $this->detailViewCount;
    }

    public function setDetailViewCount(?int $detailViewCount): self
    {
        $this->detailViewCount = $detailViewCount;

        return $this;
    }

    public function getClickViewCount(): ?int
    {
        return $this->clickViewCount;
    }

    public function setClickViewCount(?int $clickViewCount): self
    {
        $this->clickViewCount = $clickViewCount;

        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(?string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getIsLived(): ?bool
    {
        return $this->isLived;
    }

    public function setIsLived(?bool $isLived): self
    {
        $this->isLived = $isLived;

        return $this;
    }

    /**
     * @return Collection|ActiveJobCategory[]
     */
    public function getJobCategory(): Collection
    {
        return $this->jobCategory;
    }

    public function addJobCategory(ActiveJobCategory $jobCategory): self
    {
        if (!$this->jobCategory->contains($jobCategory)) {
            $this->jobCategory[] = $jobCategory;
        }

        return $this;
    }

    public function removeJobCategory(ActiveJobCategory $jobCategory): self
    {
        if ($this->jobCategory->contains($jobCategory)) {
            $this->jobCategory->removeElement($jobCategory);
        }

        return $this;
    }

    /**
     * @return Collection|ActiveJobEducationReq[]
     */
    public function getJobEducationReq(): Collection
    {
        return $this->jobEducationReq;
    }

    public function addJobEducationReq(ActiveJobEducationReq $jobEducationReq): self
    {
        if (!$this->jobEducationReq->contains($jobEducationReq)) {
            $this->jobEducationReq[] = $jobEducationReq;
        }

        return $this;
    }

    public function removeJobEducationReq(ActiveJobEducationReq $jobEducationReq): self
    {
        if ($this->jobEducationReq->contains($jobEducationReq)) {
            $this->jobEducationReq->removeElement($jobEducationReq);
        }

        return $this;
    }

    /**
     * @return Collection|ActiveJobFacility[]
     */
    public function getJobFacilities(): Collection
    {
        return $this->jobFacilities;
    }

    public function addJobFacility(ActiveJobFacility $jobFacility): self
    {
        if (!$this->jobFacilities->contains($jobFacility)) {
            $this->jobFacilities[] = $jobFacility;
            $jobFacility->setActiveJobs($this);
        }

        return $this;
    }

    public function removeJobFacility(ActiveJobFacility $jobFacility): self
    {
        if ($this->jobFacilities->contains($jobFacility)) {
            $this->jobFacilities->removeElement($jobFacility);
            // set the owning side to null (unless already changed)
            if ($jobFacility->getActiveJobs() === $this) {
                $jobFacility->setActiveJobs(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ActiveJobResponsibility[]
     */
    public function getJobResponsibilities(): Collection
    {
        return $this->jobResponsibilities;
    }

    public function addJobResponsibility(ActiveJobResponsibility $jobResponsibility): self
    {
        if (!$this->jobResponsibilities->contains($jobResponsibility)) {
            $this->jobResponsibilities[] = $jobResponsibility;
            $jobResponsibility->setActiveJobs($this);
        }

        return $this;
    }

    public function removeJobResponsibility(ActiveJobResponsibility $jobResponsibility): self
    {
        if ($this->jobResponsibility->contains($jobResponsibility)) {
            $this->jobResponsibility->removeElement($jobResponsibility);
            // set the owning side to null (unless already changed)
            if ($jobResponsibility->getActiveJobs() === $this) {
                $jobResponsibility->setActiveJobs(null);
            }
        }

        return $this;
    }

    public function getJobId(): ?string
    {
        return $this->jobId;
    }

    public function setJobId(string $jobId): self
    {
        $this->jobId = $jobId;

        return $this;
    }

    /**
     * @return Collection|ActiveSkill[]
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(ActiveSkill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
        }

        return $this;
    }

    public function removeSkill(ActiveSkill $skill): self
    {
        if ($this->skills->contains($skill)) {
            $this->skills->removeElement($skill);
        }

        return $this;
    }

    public function getSubscriptionId(): ?string
    {
        return $this->subscriptionId;
    }

    public function setSubscriptionId(?string $subscriptionId): self
    {
        $this->subscriptionId = $subscriptionId;

        return $this;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(?string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function getCompanyLogo(): ?string
    {
        return $this->companyLogo;
    }

    public function setCompanyLogo(?string $companyLogo): self
    {
        $this->companyLogo = $companyLogo;

        return $this;
    }


}
