<?php

namespace App\Repository;

use App\Entity\SearchHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SearchHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method SearchHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method SearchHistory[]    findAll()
 * @method SearchHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SearchHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SearchHistory::class);
    }

    // /**
    //  * @return SearchHistory[] Returns an array of SearchHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SearchHistory
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
	public function map($key,$value,$where = ['status'=>1]){
		$CQB = $this->createQueryBuilder('sh') ;
		$i = 0 ;
		foreach($where as $field=>$val){
			$i++;
            $CQB->andWhere('sh.'.$field.' = :val'.$i)
            ->setParameter('val'.$i, $val);
		}
        $result = $CQB->orderBy('sh.createdAt', 'DESC')->getQuery()->getResult();
		$return = [];
		
		foreach($result as $field=>$val){
			
			
			    $method1 = 'get'.ucfirst($key);
			    $method2 = 'get'.ucfirst($value);
				//$return[$val->{$method1}()] = $val->{$method2}();
				$return[] = $val->{$method2}();
			
		}
		
		return $return ;
	}
	public function mapWithLimit($key,$value,$limit = [0,10],$where = ['status'=>1]){
		$CQB = $this->createQueryBuilder('sh') ;
		$i = 0 ;
		foreach($where as $field=>$val){
			$i++;
            $CQB->andWhere('sh.'.$field.' = :val'.$i)
            ->setParameter('val'.$i, $val);
		}
        $result = $CQB->orderBy('sh.createdAt', 'DESC')
					  ->setMaxResults($limit[1])
					  ->setFirstResult($limit[0])
					  ->getQuery()
					  ->getResult();
		$return = [];
		
		foreach($result as $field=>$val){
			
			
			    $method1 = 'get'.ucfirst($key);
			    $method2 = 'get'.ucfirst($value);
				//$return[$val->{$method1}()] = $val->{$method2}();
				$return[] = $val->{$method2}();
			
		}
		
		return $return ;
	}
}
