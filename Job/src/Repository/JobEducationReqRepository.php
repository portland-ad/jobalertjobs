<?php

namespace App\Repository;

use App\Entity\JobEducationReq;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JobEducationReq|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobEducationReq|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobEducationReq[]    findAll()
 * @method JobEducationReq[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobEducationReqRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobEducationReq::class);
    }

    // /**
    //  * @return JobEducationReq[] Returns an array of JobEducationReq objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JobEducationReq
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
