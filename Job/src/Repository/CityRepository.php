<?php

namespace App\Repository;

use App\Entity\City;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, City::class);
    }

    // /**
    //  * @return City[] Returns an array of City objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?City
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    public function searchAndMapByIdName($params)
    {
        $result = $this->createQueryBuilder('o')
            ->where('o.status = :status')
            ->andWhere('o.name LIKE :cityname')
            ->setParameter('status', '1')
            ->setParameter('cityname', $params.'%')
            ->getQuery()
            ->getResult();
		$return = [] ;
		foreach($result as $key=>$val){
			//$return [$val->getId()] = $val->getName();
			$return [] = $val->getName();
		}
		
		return $return ;

    }


	public function map($key,$value,$where = ['status'=>1]){
		$CQB = $this->createQueryBuilder('c') ;
		$i = 0 ;
		foreach($where as $field=>$val){
			$i++;
            $CQB->andWhere('c.'.$field.' = :val'.$i)
            ->setParameter('val'.$i, $val);
		}
        $result = $CQB->orderBy('c.id', 'ASC')->getQuery()->getResult();
		$return = [];
		
		foreach($result as $field=>$val){
			
			
			    $method1 = 'get'.ucfirst($key);
			    $method2 = 'get'.ucfirst($value);
				$return[$val->{$method1}()] = $val->{$method2}();
			
		}
		
		return $return ;
	}
	public function mapWithLimit($key,$value,$limit = [0,10],$where = ['status'=>1]){
		$CQB = $this->createQueryBuilder('c') ;
		$i = 0 ;
		foreach($where as $field=>$val){
			$i++;
            $CQB->andWhere('c.'.$field.' = :val'.$i)
            ->setParameter('val'.$i, $val);
		}
        $result = $CQB->orderBy('c.id', 'ASC')
					  ->setMaxResults($limit[1])
					  ->setFirstResult($limit[0])
					  ->getQuery()
					  ->getResult();
		$return = [];
		
		foreach($result as $field=>$val){
			
			
			    $method1 = 'get'.ucfirst($key);
			    $method2 = 'get'.ucfirst($value);
				//$return[$val->{$method1}()] = $val->{$method2}();
				$return[] = $val->{$method2}();
			
		}
		
		return $return ;
	}
}
