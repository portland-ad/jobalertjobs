<?php

namespace App\Repository;

use App\Entity\ActiveJobCategory;
use App\Entity\JobCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ActiveJobCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActiveJobCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActiveJobCategory[]    findAll()
 * @method ActiveJobCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActiveJobCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActiveJobCategory::class);
    }

    // /**
    //  * @return JobCategory[] Returns an array of JobCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JobCategory
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
