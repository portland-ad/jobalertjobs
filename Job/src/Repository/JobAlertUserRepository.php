<?php

namespace App\Repository;

use App\Entity\JobAlertUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JobAlertUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobAlertUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobAlertUser[]    findAll()
 * @method JobAlertUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobAlertUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobAlertUser::class);
    }

    // /**
    //  * @return JobAlertUser[] Returns an array of JobAlertUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JobAlertUser
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
