<?php

namespace App\Repository;

use App\Entity\JobAlertUserEmailTracker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JobAlertUserEmailTracker|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobAlertUserEmailTracker|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobAlertUserEmailTracker[]    findAll()
 * @method JobAlertUserEmailTracker[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobAlertUserEmailTrackerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobAlertUserEmailTracker::class);
    }

    // /**
    //  * @return JobAlertUserEmailTracker[] Returns an array of JobAlertUserEmailTracker objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JobAlertUserEmailTracker
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
