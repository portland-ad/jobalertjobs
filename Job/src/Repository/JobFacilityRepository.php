<?php

namespace App\Repository;

use App\Entity\JobFacility;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JobFacility|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobFacility|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobFacility[]    findAll()
 * @method JobFacility[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobFacilityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobFacility::class);
    }

    // /**
    //  * @return JobFacily[] Returns an array of JobFacily objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JobFacily
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
