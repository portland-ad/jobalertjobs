<?php

namespace App\Repository;

use App\Entity\ActiveJobResponsibility;
use App\Entity\JobResponsibility;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ActiveJobResponsibility|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActiveJobResponsibility|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActiveJobResponsibility[]    findAll()
 * @method ActiveJobResponsibility[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActiveJobResponsibilityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActiveJobResponsibility::class);
    }

    // /**
    //  * @return JobResponsibility[] Returns an array of JobResponsibility objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JobResponsibility
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
