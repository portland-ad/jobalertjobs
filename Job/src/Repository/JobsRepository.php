<?php

namespace App\Repository;

use DateTime;
use App\Entity\Jobs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Jobs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Jobs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Jobs[]    findAll()
 * @method Jobs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobsRepository extends ServiceEntityRepository
{
    private $paginator;
    public function __construct(ManagerRegistry $registry,PaginatorInterface $paginator)
    {
        parent::__construct($registry, Jobs::class);
        $this->paginator = $paginator;
    }

    // /**
    //  * @return Jobs[] Returns an array of Jobs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    public function findByCompanyId($cID,$page =1, $limit=10){
        $connection = $this->getEntityManager()->getConnection();

        $query = "select j.id,j.job_title,j.live_at,j.end_at,j.company_id,j.is_lived,j.job_status,j.company_name,j.is_expired,count(ja.job_id) applicant,count(ja.short_listed) shortlisted from jobs j left join job_appliers ja on j.id=ja.job_id where j.company_id='".$cID."' group by j.id order by j.create_at DESC";
        /*$query = $query . "select   aj.id, aj.job_title, aj.type_in_text,aj.end_at,aj.live_at,aj.company_id,aj.company_name,aj.job_location, aj.min_salary_range,aj.city,aj.country,aj.min_salary_range,aj.currency_code,ajc.title job_category, ajc.id job_category_id from jobs aj
left join jobs_job_category joint_ajc
on aj.id = joint_ajc.jobs_id
left join  job_category ajc
on joint_ajc.job_category_id = ajc.id 
left join jobs_job_education_req ajajer
on aj.id = ajajer.jobs_id
left join job_education_req ajer
on ajajer.job_education_req_id = ajer.id
left join jobs_skill ajas
on aj.id = ajas.jobs_id
left join skill ask
on ajas.skill_id = ask.id
left join job_facility ajf
on aj.id = ajf.jobs_id
        left join job_responsibility ajr
        on aj.id = ajr.jobs_id where aj.company_id = '".$cID."' group by aj.id order by aj.create_at DESC";
*/
        $statement = $connection->prepare($query);
        $statement->execute();
        return $this->paginator->paginate($statement->fetchAll(), $page, $limit);
    }
    public function totalActiveJobsForSubscriptionCheckByUserId($userId){
		return $this->createQueryBuilder('j')->select('count(j.id)')->andWhere('j.userId = :userId')->andWhere('j.jobStatus = :status')->andWhere('j.isSubscriptionCheck = :subscriptionCheck')->setParameter('userId',$userId)->setParameter('status',1)->setParameter('subscriptionCheck',0)->getQuery()->getSingleScalarResult();
	}

    public function getUnAlertedJobByCategory($categoryid)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.isAlerted = :isAlerted')
            ->andWhere('j.categoryId = :category')
            ->setParameter('isAlerted', true)
            ->setParameter('category', $categoryid)
            ->orderBy('j.createAt', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
        ;
    }
    public function getDraftedJob($userId,$createdAtOrder = 'DESC'){

        $j = $this->createQueryBuilder('j');

           return $j->andWhere('j.jobStatus = :jobStatus')
            ->andWhere('j.userId = :userId')
           ->andWhere($j->expr()->eq('j.isExpired', 0))
           ->setParameter('jobStatus',0)
           ->setParameter('userId', $userId)
            ->orderBy('j.createAt',$createdAtOrder)
            ->getQuery();
            //->getResult();
        ;
    }

	public function search($q, $page = 1, $limit = 10){
		
		$query = $this->createQueryBuilder('J')
					//->select("J.id,J.jobTitle,J.companyName,J.jobLocation,J.education,J.experienceLevel,J.endAt,J.applied")
		              ->where("J.jobTitle LIKE :q OR J.companyName LIKE :q OR J.skillQualificationRequirement LIKE :q OR J.education LIKE :q OR J.experienceLevel LIKE :q OR J.categoryName LIKE :q OR J.minSalaryRange = :q OR J.maxSalaryRange = :q OR J.typeInText LIKE :q")
					  ->setParameter('q',"%".$q."%");
//		if(!is_null($csz) && $csz != '' && $csz != false)
//		{
//			if(is_numeric($csz))
//				$query->andWhere("J.zipCode=".$csz);
//            else{
//				$query->andWhere("J.city LIKE '%".$csz."%' OR J.state LIKE '%".$csz."%'");
//			}
//		}
        $query->orWhere("J.zipCode=".$q);
        $query->orWhere("J.city LIKE '%".$q."%' OR J.state LIKE '%".$q."%'");
		$result = $query->andWhere("J.jobStatus = 1 AND J.liveAt <= '".date('Y-m-d H:i:s')."' AND J.endAt >='".date('Y-m-d H:i:s')."'")
					  ->orderBy('J.priority', 'ASC')
                      ->orderBy('J.partialViewCount', 'ASC')
					  ->setFirstResult(($page-1)*$limit)
                       ->setMaxResults($limit)
					   ->getQuery()
					   ->getResult();
		//$paginator = new Paginator($query);
		//print_r($result);die;
		if(empty($result)) return $result ;
		return [
			'result'=>$result,
			'page'=>$page,
			'limit'=>$limit,
//		    'cache_time'=>($result[0]->getEndAt())->getTimestamp() - (new DateTime())->getTimestamp()
		];
	}
	



    public function getAllPosts($currentPage = 1, $numberOfItem,$categoryId)
    {
        $query = $this->createQueryBuilder('p')
            ->orderBy('id', 'DESC')
            ->getQuery();

        $paginator = $this->paginate($query);

        return $paginator;
    }

  public function getPaginatedJobList():Paginator
  {
      $dql = "SELECT j FROM Jobs j";
      $query = $this->createQueryBuilder('j')->createQuery($dql)
          ->setFirstResult(0)
          ->setMaxResults(100);
      return $query;

      $paginator = new Paginator($query, $fetchJoinCollection = true);
      return $paginator;
  }
  public function blockJobsByCompanyId($companyId,$block){
    $connection = $this->getEntityManager()->getConnection();

    $query = "update jobs set block =".$block." where company_id='".$companyId."'";
    $statement = $connection->prepare($query);
     return $statement->execute();
  }

}
