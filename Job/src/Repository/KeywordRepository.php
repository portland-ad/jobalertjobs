<?php

namespace App\Repository;

use App\Entity\Keyword;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Keyword|null find($id, $lockMode = null, $lockVersion = null)
 * @method Keyword|null findOneBy(array $criteria, array $orderBy = null)
 * @method Keyword[]    findAll()
 * @method Keyword[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KeywordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Keyword::class);
    }

    // /**
    //  * @return Keyword[] Returns an array of Keyword objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Keyword
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    public function findSearchKeyWord($params)
    {
           return $this->createQueryBuilder('o')
               ->where('o.status = :status')
               ->andWhere('o.keyword LIKE :keyword')
               ->setParameter('status', '1')
               ->setParameter('keyword', '%'.$params.'%')
               ->getQuery()
               ->getResult();
        ;
    }


	public function searchAndMapByIdKeyword($searchKey){
		$result = $this->createQueryBuilder('k')
            ->where('k.status = :status')
            ->andWhere('k.keyword LIKE :keyword')
            ->setParameter('status', '1')
            ->setParameter('keyword', $searchKey.'%')
            ->getQuery()
            ->getResult();
		
		$return = [] ;
		// foreach($result as $key=>$val){
			// $return [$val->getId()] = $val->getKeyword();
		// }
		foreach($result as $key=>$val){
			$return [] = $val->getKeyword();
		}
		return $return ;
	}
	public function map($key,$value,$where = ['status'=>1]){
		$CQB = $this->createQueryBuilder('k') ;
		$i = 0 ;
		foreach($where as $field=>$val){
			$i++;
            $CQB->andWhere('k.'.$field.' = :val'.$i)
            ->setParameter('val'.$i, $val);
		}
        $result = $CQB->orderBy('k.id', 'ASC')->getQuery()->getResult();
		$return = [];
		
		foreach($result as $field=>$val){
			
			
			    $method1 = 'get'.ucfirst($key);
			    $method2 = 'get'.ucfirst($value);
				$return[$val->{$method1}()] = $val->{$method2}();
			
		}
		
		return $return ;
	}
	public function mapWithLimit($key,$value,$limit = [0,10],$where = ['status'=>1]){
		$CQB = $this->createQueryBuilder('c') ;
		$i = 0 ;
		foreach($where as $field=>$val){
			$i++;
            $CQB->andWhere('c.'.$field.' = :val'.$i)
            ->setParameter('val'.$i, $val);
		}
        $result = $CQB->orderBy('c.id', 'ASC')
					  ->setMaxResults($limit[1])
					  ->setFirstResult($limit[0])
					  ->getQuery()
					  ->getResult();
		$return = [];
		
		foreach($result as $field=>$val){
			
			
			    $method1 = 'get'.ucfirst($key);
			    $method2 = 'get'.ucfirst($value);
				//$return[$val->{$method1}()] = $val->{$method2}();
				$return[] = $val->{$method2}();
			
		}
		
		return $return ;
	}
}
