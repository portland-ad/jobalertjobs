<?php

namespace App\Repository;

use App\Entity\JobAppliers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method JobAppliers|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobAppliers|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobAppliers[]    findAll()
 * @method JobAppliers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobAppliersRepository extends ServiceEntityRepository
{
	 private $paginator;
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
         parent::__construct($registry, JobAppliers::class);
        $this->paginator = $paginator;
    }
 

    // /**
    //  * @return JobAppliers[] Returns an array of JobAppliers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JobAppliers
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
	public function isThisUserApply($searchResult, $userId){

    $list = Array();
		foreach($searchResult as $key=>$val){
			if(is_object($val)){
				$jobId = $val->getId();
			}
			else{
				$jobId = $val['id'];
			}
			$applied = $this->findOneBy(['jobId'=>$jobId,'userId'=>$userId]);
			if($applied){
				if(is_object($val))
					$val->setApplied(1);
				else $val['applied'] = 1 ;
			}
			array_push($list,$val);
		}
        $searchResult->setItems($list);
		return $searchResult ;
	}
	public function alreadyApplied($jobId,$userId){
		$data = $this->findOneBy(['jobId'=>$jobId,'userId'=>$userId]);
		if($data)
		    return true;
		else return false;
	}

	public function getAllAppliedJob(string $userId, $page = 1, $limit = 10, $fromDate = null,$toDate = null,$companyName = null)
    {

        $connection = $this->getEntityManager()->getConnection();
        $q =  "select jobs.job_title,jobs.id,jobs.company_name,ja.short_listed,ja.expected_salary,ja.summary_view,ja.details_view,ja.inter_view_call,ja.currency_code,ja.created_at applied_at  from jobs inner join job_appliers ja on jobs.id = ja.job_id where ja.user_id = '".$userId."' ";
		if(!is_null($fromDate) && !is_null($toDate)){
			$q .= "and ja.created_at BETWEEN '".$fromDate."' AND '".$toDate."' ";
		}
		elseif(!is_null($fromDate)){
			$q .= "and ja.created_at = '".$fromDate."' ";
		}
		elseif(!is_null($toDate)){
			$q .= "and ja.created_at = '".$toDate."' ";
		}
		
		if(!is_null($companyName)){
			$q .= "and jobs.company_name LIKE '".$companyName."%' ";
		}
		
		$q .= " order by ja.created_at DESC";
		
        $statement = $connection->prepare($q);
        $statement->execute();
        return $this->paginator->paginate($statement->fetchAll(), $page, $limit);

    }

}
