<?php

namespace App\Repository;

use App\Entity\ActiveJobs;
use DateTime;
use App\Entity\Jobs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method ActiveJobs|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActiveJobs|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActiveJobs[]    findAll()
 * @method ActiveJobs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActiveJobsRepository extends ServiceEntityRepository
{
    private $paginator;
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, ActiveJobs::class);
        $this->paginator = $paginator;
    }

    // /**
    //  * @return Jobs[] Returns an array of Jobs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    public function searchByCompanyName($companyName,$page = 1, $limit = 10){
		
        $connection = $this->getEntityManager()->getConnection();

        $query = "";
        $query = $query . "select   aj.job_id as id, aj.max_salary_range, aj.job_title,aj.job_description, aj.type_in_text,aj.end_at,aj.live_at,aj.company_id,aj.company_name,aj.job_location, aj.min_salary_range,aj.city,aj.country,aj.min_salary_range,aj.job_title ,aj.currency_code,ajc.title job_category, ajc.id job_category_id from active_jobs aj 
left join active_jobs_active_job_category joint_ajc
on aj.id = joint_ajc.active_jobs_id
left join  active_job_category ajc
on joint_ajc.active_job_category_id = ajc.id 
left join active_jobs_active_job_education_req ajajer
on aj.id = ajajer.active_jobs_id
left join active_job_education_req ajer
on ajajer.active_job_education_req_id = ajer.id
left join active_jobs_active_skill ajas
on aj.id = ajas.active_jobs_id
left join active_skill ask
on ajas.active_skill_id = ask.id
left join active_job_facility ajf
on aj.id = ajf.active_jobs_id
        left join active_job_responsibility ajr
        on aj.id = ajr.active_jobs_id where aj.company_name LIKE '".$companyName."%' group by aj.job_id order by aj.priority ASC, aj.partial_view_count ASC";

        $statement = $connection->prepare($query);
        $statement->execute();

        return $this->paginator->paginate($statement->fetchAll(), $page, $limit);
	}
    public function findByDateExpired()
    {



        return $this->createQueryBuilder('j')
            ->select(['j.id','j.jobId'])
            ->andWhere("j.endAt < :endDate")
            ->setParameter('endDate',date('Y-m-d H:s:i'))
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
            ;
    }

    public function search($q, $page = 1, $limit = 100){

        $words_q = explode(' ',$q);
        $connection = $this->getEntityManager()->getConnection();

        $query = "";
        $query = $query . "select   aj.job_id as id, aj.max_salary_range, aj.job_title,aj.job_description, aj.type_in_text,aj.end_at,aj.live_at,aj.company_id,aj.company_name,aj.job_location, aj.min_salary_range,aj.city,aj.country,aj.min_salary_range,aj.job_title,aj.company_logo,aj.currency_code,ajc.title job_category, ajc.id job_category_id from active_jobs aj 
left join active_jobs_active_job_category joint_ajc
on aj.id = joint_ajc.active_jobs_id
left join  active_job_category ajc
on joint_ajc.active_job_category_id = ajc.id 
left join active_jobs_active_job_education_req ajajer
on aj.id = ajajer.active_jobs_id
left join active_job_education_req ajer
on ajajer.active_job_education_req_id = ajer.id
left join active_jobs_active_skill ajas
on aj.id = ajas.active_jobs_id
left join active_skill ask
on ajas.active_skill_id = ask.id
left join active_job_facility ajf
on aj.id = ajf.active_jobs_id
        left join active_job_responsibility ajr
        on aj.id = ajr.active_jobs_id ";

        $whereFlag = false;
        $orFlag = false;
        foreach($words_q as $word_q)
        {
            if(!$whereFlag)
            {
                $query = $query . "where ";
                $whereFlag = true;
            }
            else  $query = $query . "or ";

            if(is_numeric($word_q))
            {
                $query = $query . "aj.min_salary_range >= '".$word_q."'";
            }
            else
            {

                $query = $query . "aj.job_title like '%".$word_q."%' or ";
                $query = $query . "aj.type_in_text like '%".$word_q."%' or ";
                $query = $query . "aj.company_name like '%".$word_q."%' or ";
                $query = $query . "aj.job_location like '%".$word_q."%' or ";
                $query = $query . "aj.experience_level like '%".$word_q."%' or ";
                $query = $query . "aj.zip_code like '%".$word_q."%' or ";
                $query = $query . "aj.country like '%".$word_q."%' or ";
                $query = $query . "aj.state like '%".$word_q."%' or ";
                $query = $query . "ajer.degre_title like '%".$word_q."%' or ";
                $query = $query . "ask.title like '%".$word_q."%' or ";
                $query = $query . "ajf.title like '%".$word_q."%' or ";
                $query = $query . "ajc.title like '%".$word_q."%' or ";
                $query = $query . "ajr.title like '%".$word_q."%' ";

            }
        }
        $query = $query . "group by aj.job_id order by aj.priority ASC, aj.partial_view_count ASC";
        $statement = $connection->prepare($query);
        $statement->execute();
        return $this->paginator->paginate($statement->fetchAll(), $page, $limit);
    }

    public function deleteActiveJob($id)
    {

    }
	public function countExpiredJobs($dateTime){
		return $this->createQueryBuilder('j')->select('count(j.id)')->andWhere('j.endAt < :endAt')->setParameter('endAt',$dateTime)->getQuery()->getSingleScalarResult();
	}
	public function getExpiredJob($dateTime,$offset,$limit = 500){
		return $this->createQueryBuilder('j')
            ->andWhere("j.endAt < :endDate")
            ->setParameter('endDate',$dateTime)
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult()
            ;
	}
}
